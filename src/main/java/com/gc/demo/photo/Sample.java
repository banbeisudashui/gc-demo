package com.gc.demo.photo;

import com.aliyun.tea.*;
import com.aliyun.facebody20191230.*;
import com.aliyun.facebody20191230.models.*;
import com.aliyun.teaopenapi.*;
import com.aliyun.teaopenapi.models.*;

public class Sample {

    private final static String key = "LTAI5tG3V9RPdhK7WdFDMAwF";

    private final static String secret = "WIPmBiPJDZtAhM9XtIyhdSFPxuah8e";

    //private final static String endPoint = "oss-cn-shanghai.aliyuncs.com";
    private final static String endPoint = "photo-leo.oss-cn-shanghai.aliyuncs.com";

    private final static String image = "https://photo-leo.oss-cn-shanghai.aliyuncs.com/1.jpg";

    /**
     * anime：日漫风
     * 3d：3D特效
     * handdrawn：手绘风
     * sketch：铅笔画
     * artstyle：艺术特效
     */
    private final static String algotype = "anime";

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.facebody20191230.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = endPoint;
        return new com.aliyun.facebody20191230.Client(config);
    }

    public static void main(String[] args_) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.facebody20191230.Client client = Sample.createClient(key, secret);
        GenerateHumanAnimeStyleRequest generateHumanAnimeStyleRequest = new GenerateHumanAnimeStyleRequest();
        generateHumanAnimeStyleRequest.setAlgoType(algotype);
        generateHumanAnimeStyleRequest.setImageURL(image);
        // 复制代码运行请自行打印 API 的返回值
        GenerateHumanAnimeStyleResponse generateHumanAnimeStyleResponse = client.generateHumanAnimeStyle(generateHumanAnimeStyleRequest);
        System.out.println(generateHumanAnimeStyleResponse);
    }
}