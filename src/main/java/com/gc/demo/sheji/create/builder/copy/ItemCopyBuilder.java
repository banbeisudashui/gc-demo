package com.gc.demo.sheji.create.builder.copy;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 16:30
 **/
@Data
public class ItemCopyBuilder {

    private static final Long DEFAULT_STOCK = 0L;

    private String name;

    private Long stock = DEFAULT_STOCK;

    public CopyItem builder() {
        // 可以处理一些额外的逻辑
        return new CopyItem(this);
    }

    public ItemCopyBuilder setName(String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("...");
        }
        this.name = name;
        return this;
    }

    public ItemCopyBuilder setStock(Long stock) {
        if (stock > 99L) {
            throw new IllegalArgumentException("库存数量错误");
        }
        this.stock = stock;
        return this;
    }
}
