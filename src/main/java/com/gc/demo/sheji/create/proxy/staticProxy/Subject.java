package com.gc.demo.sheji.create.proxy.staticProxy;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 16:13
 **/
public interface Subject {

    void doSomething();

}
