package com.gc.demo.sheji.create.proxy.staticProxy;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 16:14
 **/
public class ProxySubject implements Subject {

    private RealSubject realSubject;

    public ProxySubject(RealSubject realSubject) {
        this.realSubject = realSubject;
    }

    @Override
    public void doSomething() {
        realSubject.doSomething();
    }

    public ProxySubject() throws Exception {
        this.realSubject  = (RealSubject) this.getClass().getClassLoader().loadClass("com.gc.demo.sheji.create.proxy.staticProxy.RealSubject").newInstance();
    }

    public static void main(String[] args) {
        try {
            // 第一种方式，类加载器方式，去加载实例对象
            new ProxySubject().doSomething();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 第二种方法，通过传值方式，把实例化对象传过来（理解为装饰器模式）
        new ProxySubject(new RealSubject()).doSomething();
        // 代理模式是提供完全相同的接口，而装饰器模式是为了增强接口。
    }
}
