package com.gc.demo.sheji.create.facotry.factory;

import com.gc.demo.sheji.create.facotry.EnumProductType;
import com.gc.demo.sheji.create.facotry.Product;
import com.gc.demo.sheji.create.facotry.Product1;
import com.gc.demo.sheji.create.facotry.Product2;

public class ProductFactory extends FactoryMethod {

    @Override
    Product createProduct(int prodId) {
        if (EnumProductType.PRODUCT_1.getId() == prodId) {
            return new Product1();
        } else {
            return new Product2();
        }
    }
}
