package com.gc.demo.sheji.create.facotry.kf;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/28 21:37
 **/
public class TestDemo {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:application.properties");
        ShareFactory shareFactory = (ShareFactory) applicationContext.getBean("shareFactory");
        Share shareFunction = shareFactory.getShareFunction(EnumShareType.SUCCESS_ORDER.getName());
        String s = shareFunction.mainProcess("123");
        System.out.println(s);
    }

}
