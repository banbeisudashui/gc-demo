package com.gc.demo.sheji.create.builder.copy;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Leo
 * @description 复制类
 * @createDate 2021/10/31 16:28
 **/
@Data
public class CopyItem implements Serializable {

    private String name;

    private Long stock;

    public CopyItem(ItemCopyBuilder itemCopyBuilder) {
        this.name = itemCopyBuilder.getName();
        this.stock = itemCopyBuilder.getStock();
    }

}
