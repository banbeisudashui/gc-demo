package com.gc.demo.sheji.create.facotry.kf;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/28 21:30
 **/
//@Component
public class ShareFactory {

    @Autowired
    private List<Share> shareFunctionList;

    public Share getShareFunction(String type) {
        for (int i = 0; i < shareFunctionList.size(); i++) {
            Share share = shareFunctionList.get(i);
            if (share.getShareFunctionType().equals(type)) {
                return share;
            }
        }
        return null;
    }

}
