package com.gc.demo.sheji.create.proxy.cglibProxy;

import com.gc.demo.sheji.create.proxy.dongtaiProxy.Cicada;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 17:04
 **/
public class PrayingMantis implements MethodInterceptor {

    private Cicada cicada;// 代理对象

    public Cicada getInstance(Cicada cicada){
        this.cicada = cicada;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(this.cicada.getClass());
        enhancer.setCallback(this);
        return (Cicada) enhancer.create();

    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        Object object  = methodProxy.invokeSuper(o, objects);
        secondaryMain();
        return object;
    }

    private void secondaryMain() {
        System.out.println("次要业务");
    }

    public static void main(String[] args) {
        PrayingMantis prayingMantis = new PrayingMantis();
        Cicada instance = prayingMantis.getInstance(new Cicada());
        instance.mainService();
        // 结果：主要业务
        //  次要业务
    }
}
