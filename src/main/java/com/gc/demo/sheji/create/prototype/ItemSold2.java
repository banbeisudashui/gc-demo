package com.gc.demo.sheji.create.prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Leo
 * @description 商品类销售量-深拷贝
 * @createDate 2021/10/31 20:29
 **/
public class ItemSold2 implements Cloneable{

    private Long itemId;
    private Long sold;

    private List<SkuSold2> skuSoldList;

    public ItemSold2() {
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        ItemSold2 clone = (ItemSold2) super.clone();
        List<SkuSold2> skuSoldList = clone.getSkuSoldList();
        List<SkuSold2> skuSolds = new ArrayList<>();
        for (int i = 0; i < skuSoldList.size(); i++) {
            SkuSold2 skuSold = skuSoldList.get(i);
            SkuSold2 cloneSkuSold = (SkuSold2) skuSold.clone();
            skuSolds.add(cloneSkuSold);
        }
        clone.setSkuSoldList(skuSolds);
        return clone;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getSold() {
        return sold;
    }

    public void setSold(Long sold) {
        this.sold = sold;
    }

    public List<SkuSold2> getSkuSoldList() {
        return skuSoldList;
    }

    public void setSkuSoldList(List<SkuSold2> skuSoldList) {
        this.skuSoldList = skuSoldList;
    }

    public ItemSold2(Long itemId, Long sold, List<SkuSold2> skuSoldList) {
        this.itemId = itemId;
        this.sold = sold;
        this.skuSoldList = skuSoldList;
    }
}
