package com.gc.demo.sheji.create.builder.demoPt;

import com.gc.demo.sheji.create.builder.Item;

/**
 * @author Leo
 * @description 普通获取商品方法
 * @createDate 2021/10/31 15:46
 **/
public class DemoTest {

    public static void main(String[] args) {
        Item item = new Item();
        int type = 0;
        // 普通商品
        if (type == 0) {
            item.setItemName("普通商品");
            item.setType(type);
        } else if (type == 1) {
            // 电子卡券商品
            item.setItemName("电子卡券商品");
            item.setType(type);
            item.setCode("123");
        } else if (type == 2) {
            // 视频商品
            item.setItemName("视频商品");
            item.setType(type);
            item.setUrl("https://......");
        } else {
            // 其他商品
            item.setItemName("其他商品");
            item.setType(type);
        }

    }

}
