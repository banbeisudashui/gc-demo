package com.gc.demo.sheji.create.proxy.dongtaiProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 16:50
 **/
public class PrayingMantis implements InvocationHandler {

    private BaseService baseService;

    public PrayingMantis(BaseService baseService) {
        this.baseService = baseService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        method.invoke(baseService, args);
        secondaryMain();
        return null;
    }

    private void secondaryMain() {
        System.out.println("次要业务方法");
    }
}
