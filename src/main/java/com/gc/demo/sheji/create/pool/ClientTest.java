package com.gc.demo.sheji.create.pool;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 15:08
 **/
public class ClientTest {
    public static void main(String[] args) {

        ClassmateBPool classmateBPool = new ClassmateBPool();
        // 初始化连接池
        classmateBPool.createPool();
        // 借钱
        Money money = classmateBPool.getMoney();
        //还钱
        classmateBPool.returnMoney(money);
        // result:对象池开始创建
        //        借钱{"money":1,"status":1}
        //        还钱成功

        // 模拟10的请求
        for (int i = 0; i < 10; i++) {
            Money money1 = classmateBPool.getMoney();
        }
        //result:
        //对象池开始创建
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //重试次数：2
        //重试次数：3
        //重试次数：4
        //Exception in thread "main" java.lang.RuntimeException:当前没有空闲的金额

        // 模拟10的请求，同时在第3次第4次的是时候还钱了
        for (int i = 0; i < 10; i++) {
            Money money1 = classmateBPool.getMoney();
            if (i == 3 || i == 4) {
                classmateBPool.returnMoney(money1);
            }
        }
        //result:
        // 对象池开始创建
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //还钱成功
        //借钱{"money":1,"status":1}
        //还钱成功
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //借钱{"money":1,"status":1}
        //重试次数：2
        //重试次数：3
        //重试次数：4
        //Exception in thread "main" java.lang.RuntimeException:当前没有空闲的金额
    }
}
