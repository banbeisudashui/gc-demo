package com.gc.demo.sheji.create.builder.demoBuilder;

import com.gc.demo.sheji.create.builder.Item;

/**
 * @author Leo
 * @description 建造者模式获取商品方法
 * @createDate 2021/10/31 15:46
 **/
public class DemoTest2 {

    public static void main(String[] args) {
        ItemBuilder itemBuilder = new ItemConcreteBuilder();
        ItemDirector itemDirector = new ItemDirector(itemBuilder);
        Item item = itemDirector.normalConstruct();
        System.out.println(item.toString());
        Item item1 = itemDirector.cardConstruct();
        System.out.println(item1.toString());
        // 省略导演类
        ItemBuilder itemBuilder2 = new ItemConcreteBuilder();
        itemBuilder2.buildCard();
        Item result = itemBuilder2.getResult();
        System.out.println(result.toString());
    }


}
