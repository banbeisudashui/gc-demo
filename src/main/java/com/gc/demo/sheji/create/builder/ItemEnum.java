package com.gc.demo.sheji.create.builder;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 15:58
 **/
public enum ItemEnum {

    NORMAL("普通商品", 1),
    CARD("电子卡券商品", 2),
    VIDEO("视频商品", 3),
    OTHER("其他商品", 4);


    private String itemName;
    private int itemId;

    ItemEnum(String itemName, int itemId) {
        this.itemName = itemName;
        this.itemId = itemId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
