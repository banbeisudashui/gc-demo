package com.gc.demo.sheji.create.proxy.staticProxy;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 16:13
 **/
public class RealSubject implements Subject {

    @Override
    public void doSomething() {
        System.out.println("放学去打球");
    }
}
