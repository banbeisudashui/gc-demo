package com.gc.demo.sheji.create.prototype;

import java.util.*;

/**
 * @author Leo
 * @description 原型模式-浅拷贝
 * @createDate 2021/10/31 20:32
 **/
public class DemoTest {

    /**
     * 浅拷贝
     * @param args
     * @throws CloneNotSupportedException
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        // 这里从缓存中获取数据
        Map<Long, ItemSold> resultMap = new HashMap<>();
        // 数据库获取数据
        List<ItemSold> resultFromDb = getResultFromDb(new Date());
        for (int i = 0; i < resultFromDb.size(); i++) {
            ItemSold itemSold = resultFromDb.get(i);
            ItemSold clone = (ItemSold) itemSold.clone();
            System.out.println(clone.getSkuSoldList());
            System.out.println(itemSold.getSkuSoldList());
            System.out.println(clone.toString());
            System.out.println(itemSold.toString());
            resultMap.put(itemSold.getItemId(), clone);
        }
    }

    /**
     * 假设这里是从数据库获取数据
     *
     * @param updateTime
     * @return
     * @throws CloneNotSupportedException
     */
    public static List<ItemSold> getResultFromDb(Date updateTime) throws CloneNotSupportedException {
        List<ItemSold> result = new ArrayList<>();
        ItemSold itemSold = new ItemSold();
        itemSold.setItemId(1L);
        itemSold.setSold(1L);
        itemSold.setSkuSoldList(Collections.singletonList(new SkuSold(3L, 3L)));
        result.add(itemSold);
        return result;
    }

}
