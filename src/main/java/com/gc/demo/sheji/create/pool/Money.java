package com.gc.demo.sheji.create.pool;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 15:07
 **/
public class Money {
    // 状态是否是被借出去
    private Integer status;
    // 金额单位 W
    private Integer money;

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Integer getMoney() {
        return money;
    }
    public void setMoney(Integer money) {
        this.money = money;
    }
}