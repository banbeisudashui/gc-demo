package com.gc.demo.sheji.create.prototype;

import java.util.List;

/**
 * @author Leo
 * @description 商品类销售量-浅拷贝
 * @createDate 2021/10/31 20:29
 **/
public class ItemSold implements Cloneable{

    private Long itemId;
    private Long sold;

    private List<SkuSold> skuSoldList;

    public ItemSold() {
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getSold() {
        return sold;
    }

    public void setSold(Long sold) {
        this.sold = sold;
    }

    public List<SkuSold> getSkuSoldList() {
        return skuSoldList;
    }

    public void setSkuSoldList(List<SkuSold> skuSoldList) {
        this.skuSoldList = skuSoldList;
    }

    public ItemSold(Long itemId, Long sold, List<SkuSold> skuSoldList) {
        this.itemId = itemId;
        this.sold = sold;
        this.skuSoldList = skuSoldList;
    }
}
