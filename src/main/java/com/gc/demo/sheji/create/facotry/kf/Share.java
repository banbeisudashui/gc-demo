package com.gc.demo.sheji.create.facotry.kf;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/28 21:28
 **/
public interface Share {

    /**
     * 获得分享类型
     * @return
     */
    String getShareFunctionType();

    /**
     *
     * @param shareName
     * @return
     */
    String mainProcess(String shareName);

}
