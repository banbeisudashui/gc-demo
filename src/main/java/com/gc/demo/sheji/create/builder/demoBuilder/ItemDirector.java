package com.gc.demo.sheji.create.builder.demoBuilder;

import com.gc.demo.sheji.create.builder.Item;

/**
 * @author Leo
 * @description 导演类，但实际上直接通过client生成
 * @createDate 2021/10/31 16:05
 **/
public class ItemDirector {

    private ItemBuilder itemBuilder;

    public ItemDirector(ItemBuilder itemBuilder) {
        this.itemBuilder = itemBuilder;
    }

    public Item normalConstruct(){
        itemBuilder.buildNormal();
        return itemBuilder.getResult();
    }


    public Item cardConstruct(){
        itemBuilder.buildCard();
        return itemBuilder.getResult();
    }


    public Item videoConstruct(){
        itemBuilder.buildVideo();
        return itemBuilder.getResult();
    }


    public Item otherConstruct(){
        itemBuilder.buildOther();
        return itemBuilder.getResult();
    }
}
