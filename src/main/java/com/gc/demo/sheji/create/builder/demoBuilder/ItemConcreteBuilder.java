package com.gc.demo.sheji.create.builder.demoBuilder;

import com.gc.demo.sheji.create.builder.ItemEnum;

/**
 * @author Leo
 * @description 具体建造者类
 * @createDate 2021/10/31 15:56
 **/
public class ItemConcreteBuilder extends ItemBuilder{

    @Override
    void buildNormal() {
        item.setItemName(ItemEnum.NORMAL.getItemName());
        item.setType(ItemEnum.NORMAL.getItemId());
    }

    @Override
    void buildCard() {
        item.setItemName(ItemEnum.CARD.getItemName());
        item.setType(ItemEnum.CARD.getItemId());
    }

    @Override
    void buildVideo() {
        item.setItemName(ItemEnum.VIDEO.getItemName());
        item.setType(ItemEnum.VIDEO.getItemId());
    }

    @Override
    void buildOther() {
        item.setItemName(ItemEnum.OTHER.getItemName());
        item.setType(ItemEnum.OTHER.getItemId());
    }

}
