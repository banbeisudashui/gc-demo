package com.gc.demo.sheji.create.facotry.kf;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/28 21:35
 **/
public class SuccessOrderShare implements Share{

    @Override
    public String getShareFunctionType() {
        return EnumShareType.SUCCESS_ORDER.getName();
    }

    @Override
    public String mainProcess(String shareName) {
        // 这里写分享的逻辑业务
        return shareName;
    }


}
