package com.gc.demo.sheji.create.facotry;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/26 22:55
 **/
public enum EnumProductType {

    PRODUCT_1(1),
    PRODUCT_2(2);

    private int id;

    EnumProductType(int i) {
        this.id = i;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
