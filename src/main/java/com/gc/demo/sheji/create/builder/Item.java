package com.gc.demo.sheji.create.builder;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 15:45
 **/
@Data
public class Item implements Serializable {

    private String itemName;

    private Integer type;

    /**
     * 卡券必填
     */
    private String code;

    /**
     * 视频必填
     */
    private String url;

}
