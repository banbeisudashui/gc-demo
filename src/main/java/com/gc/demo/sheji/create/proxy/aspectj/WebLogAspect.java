package com.gc.demo.sheji.create.proxy.aspectj;

import com.alibaba.fastjson.JSON;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Leo
 * @description aspect切面
 * @createDate 2021/11/7 15:19
 **/
@Aspect
@Component
public class WebLogAspect {

    private Logger LOG = LoggerFactory.getLogger(WebLogAspect.class);

    ThreadLocal<Long> startTime = new ThreadLocal<>();

    /**
     * 定义切入点，以controller下所有包的请求为切入点
     */
    @Pointcut("execution(public * com.gc.demo.sheji.create.proxy.aspectj.controller..*.*(..))*")
    public void webLog(){
        System.out.println("定义切入点，以controller下所有包的请求为切入点");
    }

    /**
     *前置通知：在切入点之前执行的通知
     * 在一个连接点之前执行的通知。但这种通知不能阻止连接点的执行流程（除非它抛出一个异常）
     * @param joinPoint
     * @throws Throwable
     */
    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {

        startTime.set(System.currentTimeMillis());

        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        //打印请求相关参数
        LOG.info("========================================== 前置通知Start ==========================================");


        LOG.info("URL：" + request.getRequestURL().toString());

        LOG.info("HTTP_METHOD：" + request.getMethod());

        //header第一种格式展示
        Enumeration<String> enumeration = request.getHeaderNames();
        Map<String, String> headerMap = new HashMap<>();
        while (enumeration.hasMoreElements()) {
            String headerName = enumeration.nextElement();
            headerMap.put(headerName, request.getHeader(headerName));
        }
        String headerJsonStr = JSON.toJSONString(headerMap);
        if (headerJsonStr.length() > 0) {
            LOG.info("HTTP_HEADERS INFO IS: {}", headerJsonStr);
        }
        System.out.println("header第一种格式展示结束");
        //header第二种格式展示
        LOG.info("HTTP_HEADERS: ");
        Enumeration<?> enumeration1 = request.getHeaderNames();
        while (enumeration1.hasMoreElements()) {
            String key = (String) enumeration1.nextElement();
            String value = request.getHeader(key);
            LOG.info("     {}: {}", key, value);
        }

        LOG.info("IP：" + request.getRemoteAddr());

        LOG.info("CLASS_METHOD：" + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        try {
            LOG.info("REQUEST BODY : [{}]", JSON.toJSONString(joinPoint.getArgs()[0]));
//            LOG.info("ARGS：{}", Arrays.toString(joinPoint.getArgs()));
        } catch (Exception e) {
            LOG.error("REQUEST BODY PARSE ERROR!");
        }

        HttpSession session = (HttpSession) servletRequestAttributes.resolveReference(RequestAttributes.REFERENCE_SESSION);
        LOG.info("SESSION ID：" + session.getId());
        System.out.println("header第二种格式展示结束");
        LOG.info("========================================== 前置通知end ==========================================");

    }


//    /**
//     * 后置通知
//    在一个连接点正常完成后执行的通知（如，如果一个方法没有抛出异常的返回）
//     * @param ret
//     * @throws Throwable
//     */
//    @AfterReturning(returning = "ret", pointcut = "webLog()")
//    public void doAfterReturning(Object ret) throws Throwable {
//        // 处理完请求，返回内容
//        LOG.info("RESPONSE : " + ret);
//        LOG.info("SPEND TIME : " + (System.currentTimeMillis() - startTime.get()));
//
//    }


    /**
     * 后置最终通知
     * 在一个连接点退出时（不管是正常还是异常返回）执行的通知。
     * @throws Throwable
     */
    @After("webLog()")
    public void doAfter() throws Throwable {
        LOG.info("=========================================== End ===========================================");
        // 每个请求之间空一行
        LOG.info("");
    }


    /**
     * 环绕通知
     * 环绕通知第一个参数必须是org.aspectj.lang.ProceedingJoinPoint类型
     * 环绕一个连接点的通知，比如方法的调用。这是一个最强大的通知类型。环绕通知可以在方法调用之前和之后完成自定义的行为。也负责通过返回自己的返回值或者抛出异常这些方式，选择是否继续执行连接点或者简化被通知方法的执行。
     * @param proceedingJoinPoint
     * @return
     * @throws Throwable
     */
    @Around("webLog()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = proceedingJoinPoint.proceed();
        String resultStr = JSON.toJSONString(result);
        // 打印出参
        LOG.info("RESPONSE ARGS  : {}", resultStr);
        // 执行耗时
        LOG.info("TIME-CONSUMING : {} ms", System.currentTimeMillis() - startTime);
        return result;
    }


}
