package com.gc.demo.sheji.create.proxy.dongtaiProxy;

import java.lang.reflect.Proxy;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 16:55
 **/
public class BeanFacotry {

    public static BaseService newInstanc(Class classFile){
        BaseService  trueCicada = new Cicada();
        PrayingMantis prayingMantis = new PrayingMantis(trueCicada);
        Class classArray[] = {BaseService.class};
        BaseService baseService = (BaseService) Proxy.newProxyInstance(classFile.getClassLoader(), classArray, prayingMantis);
        return baseService;
    }

    public static void main(String[] args) {
        BaseService baseService = newInstanc(Cicada.class);
        baseService.mainService();
    }

}
