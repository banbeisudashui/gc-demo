package com.gc.demo.sheji.create.builder.copy;

/**
 * @author Leo
 * @description 建造者模式-复制商品
 * @createDate 2021/10/31 15:46
 **/
public class DemoTest3 {

    public static void main(String[] args) {
        CopyItem copyItem = new ItemCopyBuilder().setName("copyItem").builder();
        System.out.println(copyItem.toString());
        CopyItem copyItem2 = new ItemCopyBuilder().setStock(100L).builder();
        System.out.println(copyItem2);
    }


}
