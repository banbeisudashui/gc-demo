package com.gc.demo.sheji.create.facotry.factory;

import com.gc.demo.sheji.create.facotry.EnumProductType;
import com.gc.demo.sheji.create.facotry.Product1;
import com.gc.demo.sheji.create.facotry.Product2;

public class FacotryTest {

    public static void main(String[] args) {
        FactoryMethod method = new ProductFactory();
        Product1 product1 = (Product1) method.createProduct(EnumProductType.PRODUCT_1.getId());
        Product2 product2 = (Product2) method.createProduct(EnumProductType.PRODUCT_2.getId());
        Product1 productA = (Product1) method.Product(EnumProductType.PRODUCT_1.getId());
        Product2 productB = (Product2) method.Product(EnumProductType.PRODUCT_2.getId());
        product1.setName("商品1");
        product2.setName("商品2");
        productA.setName("商品A");
        productB.setName("商品B");
        System.out.println(product1);
        System.out.println(product2);
        System.out.println(productA);
        System.out.println(productB);
    }

}
