package com.gc.demo.sheji.create.facotry.factory;

import com.gc.demo.sheji.create.facotry.Product;

/**
 * 总的工厂抽象类（用于被其他子工厂集成）
 */
public abstract class FactoryMethod {

    abstract Product createProduct(int prodId);

    //abstract Consumer createConsumer(int conId);

    public Product Product(int prodId) {
        Product product = createProduct(prodId);
        return product;
    }



}
