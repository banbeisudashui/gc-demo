package com.gc.demo.sheji.create.facotry.simple;

import com.gc.demo.sheji.create.facotry.EnumProductType;
import com.gc.demo.sheji.create.facotry.Product;
import com.gc.demo.sheji.create.facotry.Product1;
import com.gc.demo.sheji.create.facotry.Product2;

/**
 * @author Leo
 * @description 简单工厂模式-静态工厂
 * @createDate 2021/10/26 22:51
 **/
public class SimpleFactory1 {

    public static void main(String[] args) {
        Product product = getProduct(EnumProductType.PRODUCT_1.getId());
        System.out.println(product);
    }

    public static Product getProduct(int type) {
        if (EnumProductType.PRODUCT_1.getId() == 1) {
            return new Product1();
        } else {
            return new Product2();
        }
    }

}
