package com.gc.demo.sheji.create.facotry.kf;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/28 21:34
 **/
public enum EnumShareType {

    SUCCESS_ORDER("successOrder");

    private String name;

    EnumShareType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
