package com.gc.demo.sheji.create.facotry;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/26 22:52
 **/
@Data
public class Product1 extends Product implements Serializable {

    private String name;

}
