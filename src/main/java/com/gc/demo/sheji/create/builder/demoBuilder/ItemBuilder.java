package com.gc.demo.sheji.create.builder.demoBuilder;

import com.gc.demo.sheji.create.builder.Item;

/**
 * @author Leo
 * @description 抽象建造类
 * @createDate 2021/10/31 15:51
 **/
public abstract class ItemBuilder {

    protected Item item = new Item();

    /**
     * 创建普通商品
     */
    abstract void buildNormal();

    /**
     * 创建电子卡券商品
     */
    abstract void buildCard();

    /**
     * 创建视频商品
     */
    abstract void buildVideo();

    abstract  void buildOther();

    public Item getResult() {
        return item;
    }

}
