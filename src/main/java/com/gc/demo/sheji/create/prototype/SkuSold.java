package com.gc.demo.sheji.create.prototype;

/**
 * @author Leo
 * @description 商品类下某一个商品
 * @createDate 2021/10/31 20:30
 **/
public class SkuSold {

    private Long skuId;

    private Long skuSold;

    public SkuSold(Long skuId, Long skuSold) {
        this.skuId = skuId;
        this.skuSold = skuSold;
    }

    public SkuSold() {
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getSkuSold() {
        return skuSold;
    }

    public void setSkuSold(Long skuSold) {
        this.skuSold = skuSold;
    }

}
