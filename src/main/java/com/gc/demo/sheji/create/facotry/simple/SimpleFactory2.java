package com.gc.demo.sheji.create.facotry.simple;

import com.gc.demo.sheji.create.facotry.EnumProductType;
import com.gc.demo.sheji.create.facotry.Product;
import com.gc.demo.sheji.create.facotry.Product1;
import com.gc.demo.sheji.create.facotry.Product2;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Leo
 * @description 简单工厂模式-反射机制
 * @createDate 2021/10/26 23:00
 **/
public class SimpleFactory2 {

    private static Map<EnumProductType, Class> map = new HashMap<>();

    public static void addProduct(EnumProductType type, Class product) {
        map.put(type, product);
    }

    public static Product getProduct(EnumProductType type) throws IllegalAccessException, InstantiationException {
        return (Product) map.get(type).newInstance();
    }

    public static void main(String[] args) {
        try {
            addProduct(EnumProductType.PRODUCT_1, Product1.class);
            addProduct(EnumProductType.PRODUCT_2, Product2.class);
            Product product1 = getProduct(EnumProductType.PRODUCT_1);
            Product product2 = getProduct(EnumProductType.PRODUCT_2);
            System.out.println(product1);
            System.out.println(product2);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

}
