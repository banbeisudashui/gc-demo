package com.gc.demo.sheji.create.prototype;

import java.util.*;

/**
 * @author Leo
 * @description 原型模式-浅拷贝
 * @createDate 2021/10/31 20:32
 **/
public class DemoTest2 {

    /**
     * 浅拷贝
     * @param args
     * @throws CloneNotSupportedException
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        // 这里从缓存中获取数据
        Map<Long, ItemSold2> resultMap = new HashMap<>();
        // 数据库获取数据
        List<ItemSold2> resultFromDb = getResultFromDb(new Date());
        for (int i = 0; i < resultFromDb.size(); i++) {
            ItemSold2 itemSold = resultFromDb.get(i);
            ItemSold2 clone = (ItemSold2) itemSold.clone();
            System.out.println(clone.getSkuSoldList());
            System.out.println(itemSold.getSkuSoldList());
            System.out.println(clone.toString());
            System.out.println(itemSold.toString());
            resultMap.put(itemSold.getItemId(), clone);
        }
    }

    /**
     * 假设这里是从数据库获取数据
     *
     * @param updateTime
     * @return
     * @throws CloneNotSupportedException
     */
    public static List<ItemSold2> getResultFromDb(Date updateTime) throws CloneNotSupportedException {
        List<ItemSold2> result = new ArrayList<>();
        ItemSold2 itemSold = new ItemSold2();
        itemSold.setItemId(1L);
        itemSold.setSold(1L);
        itemSold.setSkuSoldList(Collections.singletonList(new SkuSold2(3L, 3L)));
        result.add(itemSold);
        return result;
    }

}
