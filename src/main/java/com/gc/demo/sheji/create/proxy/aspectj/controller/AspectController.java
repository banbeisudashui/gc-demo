package com.gc.demo.sheji.create.proxy.aspectj.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Leo
 * @description aspect的demo
 * @createDate 2021/11/7 15:18
 **/
@RestController
@RequestMapping("/aopTest")
public class AspectController {

    /**
     * http://localhost:8080/aopTest/sayHi/你好
     * @param name
     * @return
     */
    @RequestMapping(value = "/sayHi/{name}", method = RequestMethod.GET)
    public String sayHi(@PathVariable(value = "name") String name) {
        return "hi, " + name;
    }

}
