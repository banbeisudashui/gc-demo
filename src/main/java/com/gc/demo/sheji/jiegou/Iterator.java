package com.gc.demo.sheji.jiegou;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/4 23:43
 **/
public interface Iterator<E> {
    // 判断容器是否有值
    boolean hasNext();

    // 把游标执向下一个指针
    void next();

    // 当前遍历的数据
    E currentItem();
}