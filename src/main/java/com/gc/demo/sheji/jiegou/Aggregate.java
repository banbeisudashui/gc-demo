package com.gc.demo.sheji.jiegou;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/4 23:43
 **/
public interface Aggregate {
    // 添加元素
    void add(Object object);

    // 移除元素
    void remove(Object object);

    // 迭代器
    Iterator iterator();
}