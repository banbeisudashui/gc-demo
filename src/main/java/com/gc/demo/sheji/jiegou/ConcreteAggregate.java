package com.gc.demo.sheji.jiegou;

import java.util.ArrayList;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/4 23:43
 **/
public class ConcreteAggregate implements Aggregate {
    private ArrayList arrayList = new ArrayList();

    @Override
    public void add(Object object) {
        this.arrayList.add(object);
    }

    @Override
    public void remove(Object object) {
        this.arrayList.remove(object);
    }

    @Override
    public Iterator iterator() {
        return new ConcreteIterator(this.arrayList);
    }
}
