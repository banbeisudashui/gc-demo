package com.gc.demo.sheji.jiegou;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/4 23:44
 **/
public class ConcreteIterator<E> implements Iterator<E> {

    private int cursor; // 游标
    private ArrayList arrayList;

    public ConcreteIterator(ArrayList arrayList) {
        this.cursor = 0;
        this.arrayList = arrayList;
    }

    @Override
    public boolean hasNext() {
        if (this.cursor == this.arrayList.size()) {
            return false;
        }
        return true;
    }

    @Override
    public void next() {
        cursor++;
        System.out.println(cursor + "   cursor");
    }

    @Override
    public E currentItem() {
        if (cursor >= arrayList.size()) {
            throw new NoSuchElementException();
        }
        E e = (E) arrayList.get(cursor);
        this.next();
        return e;
    }

    // 测试demo
    public static void main(String[] args) {
        Aggregate aggregate = new ConcreteAggregate();
        aggregate.add("java");
        aggregate.add("c++");
        aggregate.add("php");
        aggregate.add("敖丙");

        Iterator iterator = aggregate.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.currentItem());
        }
        // 结果：1    java
        //      2     c++
        //      3     php
        //      4     敖丙
    }
}
