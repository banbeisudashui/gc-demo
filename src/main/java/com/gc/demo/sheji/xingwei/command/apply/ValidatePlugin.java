package com.gc.demo.sheji.xingwei.command.apply;

/**
 * @author Leo
 * @description 定义抽象校验方法
 * @createDate 2021/11/5 23:05
 **/

public abstract class ValidatePlugin {
    public abstract void validate();
}
