package com.gc.demo.sheji.xingwei.muban;

public abstract class AskForLeaveFlow {

    //  一级组长直接审批
    protected abstract void firstGroupLeader(String name);

    // 二级组长直接审批
    protected void secondGroupLeader(String name) {
    }

    private final void notifyHr(String name) {
        System.out.println("当前有人请假了，请假人:" + name);
    }

    // 请假流模板
    public void askForLeave(String name) {
        firstGroupLeader(name);
        secondGroupLeader(name);
        notifyHr(name);
    }

}
