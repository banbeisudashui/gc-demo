package com.gc.demo.sheji.xingwei.celue.pt.apply;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/3 22:26
 **/
public interface ShareStrategy {

    public void shareAlgorithm(String param);
}
