package com.gc.demo.sheji.xingwei.guanchazhe;

/**
 * @author Leo
 * @description 观察者接口
 * @createDate 2021/11/1 22:36
 **/
public interface Observer {

    /**
     * 业务逻辑
     */
    void update(String message);

}
