package com.gc.demo.sheji.xingwei.handler.apply;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Leo
 * @description 抽象类处理器
 * @createDate 2021/10/31 22:13
 **/
public abstract class AbstractDataHandler<T> {

    protected abstract T doRequest(HttpServletRequest request, String query) throws Exception;

}
