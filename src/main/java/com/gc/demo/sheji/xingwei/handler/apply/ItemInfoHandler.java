package com.gc.demo.sheji.xingwei.handler.apply;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 22:14
 **/
@Component
public class ItemInfoHandler extends AbstractDataHandler<ItemInfo> {
    @Override
    protected ItemInfo doRequest(HttpServletRequest request,String query) throws Exception {
        ItemInfo itemInfo = new ItemInfo();
        itemInfo.setItemId(123456L);
        itemInfo.setItemName("测试商品");
        return itemInfo;
    }
}
