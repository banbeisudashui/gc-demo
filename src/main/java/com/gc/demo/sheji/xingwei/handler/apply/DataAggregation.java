package com.gc.demo.sheji.xingwei.handler.apply;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 22:19
 **/
@Component
public class DataAggregation {

    @Autowired
    private SkuInfoHandler skuInfoHandler;
    @Autowired
    private ItemInfoHandler itemInfoHandler;

    public Map convertItemDetail(HttpServletRequest request) throws Exception {
        Map result = new HashMap();
        result.put("skuInfoHandler", skuInfoHandler.doRequest(request, "模拟数据请求"));
        result.put("itemInfoHandler", itemInfoHandler.doRequest(request, "模拟数据请求"));
        return result;
    }

}
