package com.gc.demo.sheji.xingwei.guanchazhe;

/**
 * @author Leo
 * @description 具体观察者
 * @createDate 2021/11/1 22:40
 **/
public class FriendOneObserver implements Observer{
    @Override
    public void update(String message) {
        System.out.println("FriendOne 知道了你发动态了" + message);
    }
}
