package com.gc.demo.sheji.xingwei.zhongjie;

import com.gc.demo.sheji.xingwei.command.apply.TestValidatePlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/5 23:10
 **/
@RestController
@RequestMapping("demo")
public class DemoController {

    @Autowired
    private DispatchCenter dispatchCenter;

    @Autowired
    private MotorCarOneColleague oneColleague;

    @Autowired
    private MotorCarTwoColleague twoColleague;

    @RequestMapping("testZJ")
    public String testZJ() {
        dispatchCenter.doEvent(oneColleague);
        dispatchCenter.doEvent(twoColleague);
        return "";
    }
}