package com.gc.demo.sheji.xingwei.guanchazhe;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Leo
 * @description 具体主题
 * @createDate 2021/11/1 22:37
 **/
public class ConcreteSubject implements Subject {

    // 订阅者容器
    private List<Observer> observers = new ArrayList<Observer>();

    /**
     * 添加订阅者
     *
     * @param observer
     */
    @Override
    public void attach(Observer observer) {
        observers.add(observer);
    }

    /**
     * 删除订阅者
     *
     * @param observer
     */
    @Override
    public void detach(Observer observer) {
        observers.remove(observer);
    }

    /**
     * 通知订阅者
     *
     * @param message
     */
    @Override
    public void notifyObserver(String message) {
        for (int i = 0; i < observers.size(); i++) {
            Observer observer = observers.get(i);
            observer.update(message);
        }
    }
}
