package com.gc.demo.sheji.xingwei.celue;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/3 22:02
 **/
public abstract class GearStrategyAbstract {

    // 定义策略执行方法
    abstract  void algorithm(String param);


}
