package com.gc.demo.sheji.xingwei.command;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/5 22:58
 **/
public class Invoker {

    private Command command;

    public Invoker(Command command) {
        this.command = command;
    }

    public void excute(){
        command.excute();
    }
}
