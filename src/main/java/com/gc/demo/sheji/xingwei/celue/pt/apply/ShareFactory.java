package com.gc.demo.sheji.xingwei.celue.pt.apply;


import java.util.HashMap;
import java.util.Map;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/3 22:26
 **/
public class ShareFactory {

    private static final Map<String, ShareStrategy> shareStrategies = new HashMap<>();

    static {
        shareStrategies.put("order", new OrderItemShare());
        shareStrategies.put("single", new SingleItemShare());
        shareStrategies.put("multi", new MultiItemShare());
    }

    // 获取指定策略
    public static ShareStrategy getShareStrategy(String type) {
        if (type == null || type.isEmpty()) {
            throw new IllegalArgumentException("type should not be empty.");
        }
        return shareStrategies.get(type);
    }

    public static void main(String[] args) {
        // 测试demo
        String shareType = "order";
        ShareStrategy shareStrategy = ShareFactory.getShareStrategy(shareType);
        shareStrategy.shareAlgorithm("order");
        // 输出结果：当前分享图片是order
    }

}
