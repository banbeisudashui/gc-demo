package com.gc.demo.sheji.xingwei.command;

/**
 * @author Leo
 * @description 抽象命令类
 * @createDate 2021/11/5 22:48
 **/
public interface Command {

    void excute();

}
