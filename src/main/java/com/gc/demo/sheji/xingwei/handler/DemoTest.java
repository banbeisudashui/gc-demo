package com.gc.demo.sheji.xingwei.handler;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 21:49
 **/
public class DemoTest {

    public static void main(String[] args) {
        Handler firstHandler = new FirstHandler();
        Handler threeHandler = new ThreeHandler();
        firstHandler.setHandler(threeHandler);
        firstHandler.handleRequest(1);
        firstHandler.handleRequest(3);

    }

}
