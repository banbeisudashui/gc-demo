package com.gc.demo.sheji.xingwei.guanchazhe;

/**
 * @author Leo
 * @description 定义主题
 * @createDate 2021/11/1 22:33
 **/
public interface Subject {

    /**
     * 添加订阅关系
     *
     * @param observer
     */
    void attach(Observer observer);

    /**
     * 删除订阅关系
     *
     * @param observer
     */
    void detach(Observer observer);

    /**
     * 通知订阅者
     *
     * @param message
     */
    void notifyObserver(String message);

}
