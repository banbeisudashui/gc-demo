package com.gc.demo.sheji.xingwei.jsq;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 15:20
 **/
// 乘法表达式
public class MultiplyExpression implements Expression {
    Expression left;
    Expression right;
    public MultiplyExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Long interpret() {
        return left.interpret() * right.interpret();
    }
}