package com.gc.demo.sheji.xingwei.zhongjie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Leo
 * @description具体中介者
 * @createDate 2021/11/5 23:31
 **/
@Component
public class DispatchCenter implements Mediator {
    // 管理有哪些参与者
    @Autowired
    private List<Colleague> colleagues;

    @Override
    public void doEvent(Colleague colleague) {
        for(Colleague colleague1 :colleagues){
            if(colleague1==colleague){
                // 如果是本身高铁信息，可以处理其他的业务逻辑
                // doSomeThing();
                continue;
            }
            // 通知其他参与
            colleague1.message();
        }
    }
}