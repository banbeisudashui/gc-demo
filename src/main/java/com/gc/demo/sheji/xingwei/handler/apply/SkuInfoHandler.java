package com.gc.demo.sheji.xingwei.handler.apply;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 22:14
 **/
@Component
public class SkuInfoHandler extends AbstractDataHandler<SkuInfo> {
    @Override
    protected SkuInfo doRequest(HttpServletRequest request,String query) throws Exception {
        SkuInfo skuInfo = new SkuInfo();
        skuInfo.setItemId(789L);
        skuInfo.setItemName("测试Sku商品");
        return skuInfo;
    }
}
