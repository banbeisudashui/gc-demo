package com.gc.demo.sheji.xingwei.guanchazhe;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/1 22:41
 **/
public class Test {

    public static void main(String[] args) {
        ConcreteSubject subject = new ConcreteSubject();
        FriendOneObserver friendOneObserver = new FriendOneObserver();
        subject.attach(friendOneObserver);
        FriendOneObserver friendOneObserver2 = new FriendOneObserver();
        subject.attach(friendOneObserver2);
        subject.notifyObserver("发布了第一条朋友圈");
        subject.detach(friendOneObserver2);
        subject.notifyObserver("发布了第二条朋友圈");
    }

}
