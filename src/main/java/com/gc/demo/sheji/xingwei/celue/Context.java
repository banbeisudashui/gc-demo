package com.gc.demo.sheji.xingwei.celue;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author Leo
 * @description 策略模式
 * @createDate 2021/11/3 22:06
 **/
public class Context {

    private static final Map<String, GearStrategyAbstract> strategies = new HashMap<>();

    static {
        strategies.put("one", new GearStrategyOne());
    }

    public static GearStrategyAbstract getStrategy(String type) {
        if (type == null || type.isEmpty()) {
            throw new IllegalArgumentException("type should not be empty.");
        }
        return strategies.get(type);
    }

    public static void main(String[] args) {
        GearStrategyAbstract one = Context.getStrategy("one");
        one.algorithm("1档");
    }


}
