package com.gc.demo.sheji.xingwei.command.apply;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/5 23:10
 **/
@RestController
@RequestMapping("demo")
public class CommandDemoController {

    @Autowired
    private TestValidatePlugin testValidatePlugin;

    @RequestMapping("testValidatePlugin")
    public String testValidatePlugin() {
        try {
            testValidatePlugin.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
