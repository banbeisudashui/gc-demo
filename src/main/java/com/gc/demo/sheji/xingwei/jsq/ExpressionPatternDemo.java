package com.gc.demo.sheji.xingwei.jsq;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 15:23
 **/
public class ExpressionPatternDemo {
    public static void main(String[] args) {
        Context context = new Context();
        Long result = context.ExpressionInterpreter("6 6 + 3 *");
        System.out.println(result);
        // result : 36
        Context contextTwo = new Context();
        Long resultTwo = contextTwo.ExpressionInterpreter("2 3 * 6 +");
        System.out.println(resultTwo);
        // result : 12
    }
}