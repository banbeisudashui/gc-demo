package com.gc.demo.sheji.xingwei.command;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/5 22:49
 **/
public class ConcreteCommandTwo implements Command{

    private Receiver receiver;

    public ConcreteCommandTwo(Receiver receiver) {
        this.receiver = receiver;
    }


    @Override
    public void excute() {
        receiver.issue();
    }
}
