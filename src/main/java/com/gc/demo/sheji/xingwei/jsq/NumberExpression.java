package com.gc.demo.sheji.xingwei.jsq;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 15:20
 **/
// 数字表达式
public class NumberExpression implements Expression {
    private Long number;
    public NumberExpression(Long number) {
        this.number = number;
    }

    @Override
    public Long interpret() {
        return number;
    }
}