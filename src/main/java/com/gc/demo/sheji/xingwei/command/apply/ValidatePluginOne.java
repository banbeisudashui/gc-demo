package com.gc.demo.sheji.xingwei.command.apply;

import org.springframework.stereotype.Component;

/**
 * @author Leo
 * @description  具体测试规则
 * @createDate 2021/11/5 23:07
 **/
@Component
public class ValidatePluginOne extends ValidatePlugin {

    @Override
    public void validate() {
        System.out.println("one 的 校验规则");
    }
}
