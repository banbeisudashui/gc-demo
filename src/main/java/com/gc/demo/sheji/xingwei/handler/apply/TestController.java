package com.gc.demo.sheji.xingwei.handler.apply;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Leo
 * @description 实际应用的责任链模式
 * @createDate 2021/10/31 22:19
 **/
@RestController
@RequestMapping("sheji")
public class TestController {

    @Autowired
    private DataAggregation dataAggregation;

    /**
     * http://localhost:8080/sheji/applyZrl
     * @return
     */
    @RequestMapping("applyZrl")
    public String applyZrl(HttpServletRequest request) {
        try {
            Map map = dataAggregation.convertItemDetail(request);
            System.out.println(JSONObject.toJSONString(map));
        } catch (Exception e) {
            e.printStackTrace();

        }
        return "责任链模式";
    }

}
