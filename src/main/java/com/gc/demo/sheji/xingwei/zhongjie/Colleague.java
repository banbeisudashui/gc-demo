package com.gc.demo.sheji.xingwei.zhongjie;

/**
 * @author Leo
 * @description 抽象参与者，也可以用abstract
 * @createDate 2021/11/5 23:27
 **/
public interface Colleague {
    // 沟通消息
    void message();
}
