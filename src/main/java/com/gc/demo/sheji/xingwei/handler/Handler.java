package com.gc.demo.sheji.xingwei.handler;

/**
 * @author Leo
 * @description 普通应用责任链模式
 * @createDate 2021/10/31 21:46
 **/
public abstract class Handler {

    protected Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public abstract void handleRequest(Integer times);
}
