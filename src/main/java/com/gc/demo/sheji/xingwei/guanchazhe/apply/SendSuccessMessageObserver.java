package com.gc.demo.sheji.xingwei.guanchazhe.apply;

import com.gc.demo.sheji.xingwei.guanchazhe.ConcreteSubject;
import com.gc.demo.sheji.xingwei.guanchazhe.Observer;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/1 23:08
 **/
public class SendSuccessMessageObserver implements Observer {

    @Override
    public void update(String message) {
        // 处理业务逻辑
        System.out.println("注册成功");
    }

    public static void main(String[] args) {
        // 假设用户注册成功直接通知观察者，改干自己的事情了
        ConcreteSubject subject = buildSubject();
        subject.notifyObserver("");
    }

    private static ConcreteSubject buildSubject() {
        ConcreteSubject subject = new ConcreteSubject();
        subject.attach(new SendSuccessMessageObserver());
        subject.attach(new SendNewPersonCouponObserver());
        return subject;
    }
}
