package com.gc.demo.sheji.xingwei.celue.pt;

/**
 * @author Leo
 * @description 传统做法
 * @createDate 2021/11/3 22:24
 **/
public class ShareFactory {

    public static void main(String[] args) throws Exception {
        Integer shareType = 1;
        // 测试业务逻辑
        if (shareType.equals(ShareType.SINGLE.getCode())) {
            SingleItemShare singleItemShare = new SingleItemShare();
            singleItemShare.algorithm("单商品");
        } else if (shareType.equals(ShareType.MULTI.getCode())) {
            MultiItemShare multiItemShare = new MultiItemShare();
            multiItemShare.algorithm("多商品");
        } else if (shareType.equals(ShareType.ORDER.getCode())) {
            OrderItemShare orderItemShare = new OrderItemShare();
            orderItemShare.algorithm("下单");
        } else {
            throw new Exception("未知分享类型");
        }
        // .....省略更多分享场景
    }

}
