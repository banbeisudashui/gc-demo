package com.gc.demo.sheji.xingwei.handler;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 21:49
 **/
public class FirstHandler extends Handler {
    @Override
    public void handleRequest(Integer times) {
        if (times == 1) {
            System.out.println("第一个处理器");
        }
        handler.handleRequest(times);
    }
}
