package com.gc.demo.sheji.xingwei.celue;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/3 22:04
 **/
public class GearStrategyOne extends GearStrategyAbstract {

    @Override
    void algorithm(String param) {
        System.out.println("当前档位" + param);
    }

}
