package com.gc.demo.sheji.xingwei.command;

/**
 * @author Leo
 * @description 命令接收者-公公
 * @createDate 2021/11/5 22:48
 **/
public class Receiver {

    public void charge(){
        System.out.println("收取奏折");
    }

    public void issue(){
        System.out.println("发放奏折");
    }

}
