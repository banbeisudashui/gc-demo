package com.gc.demo.sheji.xingwei.zhongjie;

import org.springframework.stereotype.Component;

/**
 * @author Leo
 * @description 具体参与者
 * @createDate 2021/11/5 23:28
 **/
@Component
public class MotorCarTwoColleague implements Colleague{
    @Override
    public void message() {
        System.out.println("高铁二号收到消息！！！");
    }
}
