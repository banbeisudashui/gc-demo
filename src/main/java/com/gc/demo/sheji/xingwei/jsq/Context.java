package com.gc.demo.sheji.xingwei.jsq;


import java.util.Deque;
import java.util.LinkedList;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 15:20
 **/
public class Context {

    private Deque<Expression> numbers = new LinkedList<>();

    public Long ExpressionInterpreter(String expression) {
        Long result = null;
        for (String ex : expression.split(" ")) {
            if (isOperator(ex)) {
                Expression exp = null;
                if (ex.equals("+")) {
                    exp = new MultiplyExpression(numbers.pollFirst(), numbers.pollFirst());
                }
                if (ex.equals("*")) {
                    exp = new MultiplyExpression(numbers.pollFirst(), numbers.pollFirst());
                }
                // 当还有其他的运算符时，接着在这里添加就行了

                if (null != exp) {
                    result = exp.interpret();
                    numbers.addFirst(new NumberExpression(result));
                }
            }
            if (isNumber(ex)) {
                numbers.addLast(new NumberExpression(Long.parseLong(ex)));
            }
        }
        return result;
    }

    // 判断是否是运算符号，这里举例只用了➕和✖️，可以扩展更多的其它运算符
    private boolean isOperator(String ex) {
        return ex.equals("+") || ex.equals("*");
    }

    // 判断是否是数字
    private boolean isNumber(String ex) {
        return ex.chars().allMatch(Character::isDigit);
    }
}