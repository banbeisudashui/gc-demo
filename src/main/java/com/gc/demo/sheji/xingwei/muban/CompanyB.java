package com.gc.demo.sheji.xingwei.muban;

public class CompanyB extends AskForLeaveFlow {

    @Override
    protected void firstGroupLeader(String name) {
        System.out.println("CompanyB组内有人请假，请假人：" + name);
    }
}
