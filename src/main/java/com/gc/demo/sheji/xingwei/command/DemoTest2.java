package com.gc.demo.sheji.xingwei.command;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Leo
 * @description 使用队列
 * @createDate 2021/11/5 22:51
 **/
public class DemoTest2 {

    public static void main(String[] args) {
        Receiver receiver = new Receiver();
        ConcreteCommandOne concreteCommandOne = new ConcreteCommandOne(receiver);
        ConcreteCommandTwo concreteCommandTwo = new ConcreteCommandTwo(receiver);
        Queue<Command> queue = new LinkedList<>();
        queue.add(concreteCommandOne);
        queue.add(concreteCommandTwo);
        // 批量执行
        for (Command command : queue) {
            Invoker invoker = new Invoker(command);
            invoker.excute();
        }

    }

}
