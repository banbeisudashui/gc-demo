package com.gc.demo.sheji.xingwei.celue.pt;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/3 22:25
 **/
public enum ShareType {
    SINGLE(1, "单商品"),
    MULTI(2, "多商品"),
    ORDER(3, "下单");
    /**
     * 场景对应的编码
     */
    private Integer code;
    /**
     * 业务场景描述
     */
    private String desc;

    ShareType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    // 省略 get set 方法
}
