package com.gc.demo.sheji.xingwei.command;

/**
 * @author Leo
 * @description 没有使用队列
 * @createDate 2021/11/5 22:51
 **/
public class DemoTest {

    public static void main(String[] args) {
        Receiver receiver = new Receiver();
        ConcreteCommandOne concreteCommandOne = new ConcreteCommandOne(receiver);
        ConcreteCommandTwo concreteCommandTwo = new ConcreteCommandTwo(receiver);
        Invoker invoker = new Invoker(concreteCommandOne);
        invoker.excute();
        Invoker invokerTwo = new Invoker(concreteCommandTwo);
        invokerTwo.excute();

    }

}
