package com.gc.demo.sheji.xingwei.zhongjie;

/**
 * @author Leo
 * @description 抽象中介者
 * @createDate 2021/11/5 23:27
 **/
public interface Mediator {

    // 定义处理逻辑
    void doEvent(Colleague colleague);

}
