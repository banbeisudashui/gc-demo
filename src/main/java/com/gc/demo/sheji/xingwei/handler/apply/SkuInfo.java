package com.gc.demo.sheji.xingwei.handler.apply;

import lombok.Data;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 22:14
 **/
@Data
public  class SkuInfo {
    private Long itemId;
    private String itemName;
}