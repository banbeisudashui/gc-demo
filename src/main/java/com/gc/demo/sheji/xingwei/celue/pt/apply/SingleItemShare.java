package com.gc.demo.sheji.xingwei.celue.pt.apply;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/3 22:24
 **/
public class SingleItemShare implements  ShareStrategy{
    @Override
    public void shareAlgorithm(String param) {
        System.out.println("当前分享图片是" + param);
    }
}