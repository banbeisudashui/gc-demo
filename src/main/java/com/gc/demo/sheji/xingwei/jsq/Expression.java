package com.gc.demo.sheji.xingwei.jsq;

/**
 * @author Leo
 * @description
 * @createDate 2021/11/6 15:20
 **/
// 定义抽象表达式
public interface Expression {
    Long interpret();
}