package com.gc.demo.sheji.xingwei.handler;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/31 21:49
 **/
public class ThreeHandler extends Handler {
    @Override
    public void handleRequest(Integer times) {
        if (times == 3) {
            System.out.println("第三个处理器");
        }
    }
}
