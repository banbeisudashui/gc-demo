package com.gc.demo.finalize;

/**
 * @author Leo
 * @description
 * @createDate 2021/8/24 22:38
 * -Xms10m
 * -Xmx10m
 * -verbose:gc
 * -XX:+HeapDumpOnOutOfMemoryError
 * -XX:HeapDumpPath=D:\kf\code\log\gc\a.dump
 **/
public class FinalizeGcDemo {

    private static class GcDemo {
        private byte[] content = new byte[1024 * 1024];

        @Override
        protected void finalize() throws Throwable {
            System.out.println("执行了finalize方法");
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            GcDemo gcDemo = new GcDemo();
        }
    }

}
