package com.gc.demo.finalize.finlly;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Leo
 * @description
 * @createDate 2021/8/27 10:34
 **/
@Slf4j
public class FinallyDemo {

    public static void main(String[] args) {
        try {
            int i = 1 / 0;
            log.info("1/0");
        } catch (Exception e) {
            log.info("执行异常");
        } finally {
            log.info("执行finally片段");
        }
    }

}
