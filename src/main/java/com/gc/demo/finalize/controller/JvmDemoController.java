package com.gc.demo.finalize.controller;

import com.gc.demo.thread.WaitThread;
import com.gc.demo.thread.dieLock.DieLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Leo
 * @description
 * @createDate 2021/8/28 20:58
 **/
@RestController
@Slf4j
@RequestMapping("jvm")
public class JvmDemoController {

    @RequestMapping("demo")
    public String demo() {
        return "test";
    }

    /**
     * 死锁，用于Jstack日志打印
     *
     * @return
     */
    @RequestMapping("dealLock")
    public String dealLock() {
        DieLock d1 = new DieLock(true);
        DieLock d2 = new DieLock(false);
        Thread t1 = new Thread(d1);
        Thread t2 = new Thread(d2);
        t1.start();
        t2.start();
        return "dealLock";
    }

    /**
     * 进入等待状态
     *
     * @return
     */
    @RequestMapping("waitThread")
    public String waitThread() {
        WaitThread waitThread = new WaitThread();
        Thread thread = new Thread(waitThread);
        synchronized (thread) {
            thread.start();
            System.out.println("controller");
            try {
                thread.wait(5000000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return "waitThread";
    }

}
