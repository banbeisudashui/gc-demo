package com.gc.demo;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 获得某个工程的代码量(除去空白行)
 * 可统计java jsp xml 等代码行数。
 * @author qiuy
 */
public class LineNumberReaderUtils {

    /**
     * 得到指定java文件行数
     * @param path
     * @return
     */
    public static int getAllLineNumber(String path){

        int result = 0;
        File file = new File(path);
        InputStream is = null;
        InputStreamReader reader = null;
        LineNumberReader lineNumberReader = null;
        try {
            is = new FileInputStream(file);
            reader = new InputStreamReader(is);
            @SuppressWarnings("unused")
            String strTemp = "";
            lineNumberReader = new LineNumberReader(reader);
            while((strTemp = lineNumberReader.readLine()) != null){
                if (!"".equals(strTemp.trim())) {
                    result++;
                }
            }
        } catch (Exception e) {

        }
        return result;
    }
    /**
     *
     * @param path 目录路径
     * @param type 得到文件路径
     * @param needSub 是否需要递归
     * @param list 装载所有type类型的文件list
     * @return
     */
    public static List<String> getAllFileByPath(String path,String type,boolean needSub,List<String> list){
        String[] files = null;
        File file = new File(path);
        files = file.list();
        File fileRoot = null;
        String fileName = "";
        String fileType = "";
        for (int i = 0; i < files.length; i++) {
            fileRoot = new File(path+File.separator+files[i]);
            if (!fileRoot.isDirectory()) {
                fileName =fileRoot.getName();
                if (fileName.indexOf(".") != -1) {
                    fileType = fileName.substring(fileName.lastIndexOf("."));
                }
                if (fileType.equals(type)) {
                    list.add(fileRoot.getAbsolutePath());
                }
            }
            if (needSub && fileRoot.isDirectory()) {
                getAllFileByPath(fileRoot.getAbsolutePath(), type, needSub,list);
            }

        }

        return list;
    }
    /**
     *
     * @param path 目录路径
     * @param type 文件类型
     * @param needSub 是否需要递归
     * @return
     */
    public static int getAllLineNumberByPath(String path,String type ,boolean needSub){
        if (path == null || "".equals(path.trim()) || type == null || "".equals(type.trim())) {
            return 0;
        }
        List<String> list = getAllFileByPath(path, type, needSub, new ArrayList<String>());
        int allLineNumberCount = 0;
        String filePath= "";
        for (Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
            filePath = iterator.next();
            allLineNumberCount += getAllLineNumber(filePath);
        }
        return allLineNumberCount;
    }

    /**
     * 测试main方法
     * @param args
     */
    public static void main(String[] args) {

        //参数说明
        //  1 "D:\\javaWorkPlace\\rose-frame" 在这个目录下面的文件
        //  2 ".java"要数行数的目标文件 如果统计jsp就改成.jsp
        //  3 true表示需要目录下中所有java文件（支持递归）
        int count = getAllLineNumberByPath("D:\\code\\gitlab\\person-api", ".java", true);
        System.out.println(count);
    }
}
