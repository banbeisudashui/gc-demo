package com.gc.demo.engine;

import com.gc.demo.engine.config.FlowEngine;
import com.gc.demo.engine.config.FlowNode;
import com.gc.demo.engine.model.Context;
import com.gc.demo.engine.model.RunData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/30 15:49
 **/
@Slf4j
@RestController
@RequestMapping("engine")
public class EngineController {

    @Autowired
    private FlowEngine flowEngine;

    /**
     * http://localhost:8080/engine/demo
     * @return
     */
    @RequestMapping("demo")
    public String demo() {
        // 生成需要校验的节点组及节点
        FlowNode testFlow = Flow.getTestFlow();
        RunData runData = new RunData();
        runData.setParamOne("one");
        runData.setParamTwo("two");
        Context context = new Context();
        try {
            flowEngine.execute(testFlow, runData, context);
            Map<String, Object> adptorMap = context.getAdptorMap();
            System.out.println(adptorMap.get("NodeOne"));
            System.out.println(adptorMap.get("NodeTwo"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "测试完毕~！";
    }

}
