package com.gc.demo.engine.config;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/30 15:10
 **/
@Component
public class BeanService implements ApplicationContextAware {

    /**
     * spring bean 上下文
     */
    protected static ApplicationContext applicationContext = null;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        BeanService.applicationContext = applicationContext;
    }

    public static Object getBeanName(String name) {
        return applicationContext.getBean(name);
    }

    /**
     * 根据class 获取bean
     * @param clazz
     * @param <T>
     *           BeansException 当继承或接口时（多个实现类）getBean(clazz)会报错
     *           所以通过classname 比较获取唯一bean
     * @return
     */
    public static <T> T getSingleBeanByType(Class<T> clazz) {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanName : beanDefinitionNames) {
            Object beanByName = getBeanName(beanName);
            if (beanByName != null) {
                String name = clazz.getName();
                Class<?> targetClass = AopUtils.getTargetClass(beanByName);
                if(name.equals(targetClass.getName())){
                    return (T) beanByName;
                }
            }
        }
        throw new RuntimeException();
    }
}
