package com.gc.demo.engine.config;

import com.gc.demo.engine.model.Context;
import com.gc.demo.engine.model.NodeConf;
import com.gc.demo.engine.model.RunData;
import com.gc.demo.engine.service.FlowNodeInterface;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.*;

/**
 * @author Leo
 * @description 引擎入口
 * @createDate 2021/10/30 14:40
 **/
@Service
public class FlowEngine {

    public void execute(FlowNode flowNode, RunData runData, Context context) throws Exception {
        // 节点分组
        Map<String, List<String>> nodeGroup = groupByGroupName(flowNode);
        // 获取节点配置
        Map<String, NodeConf> nodeMap = flowNode.getNodeMap();
        // 节点名/组名
        Set<String> keySet = nodeGroup.keySet();
        for (String groupName : keySet) {
            boolean needThrowExp = false;
            List<String> nodeNameList = nodeGroup.get(groupName);
            // 如果只有一个节点，串行执行
            if (nodeNameList.size() == 1) {
                String nodeName = nodeNameList.get(0);
                FlowNodeInterface detailNode = (FlowNodeInterface) BeanService.getSingleBeanByType(Class.forName(nodeName));
                NodeExecuteTask nodeParallelTask = new NodeExecuteTask(detailNode, runData, context);
                try {
                    Object result = nodeParallelTask.excute();
                    context.getAdptorMap().put(detailNode.resultKey(), result);
                } catch (Exception e) {
                    needThrowExp = true;
                }
            } else {
                // 多个节点，并行执行
                // 节点结果
                List<Future> resultList = new ArrayList<>();
                // 需要执行的节点
                List<String> executeNodeNameList = new ArrayList<>();
                List<NodeExecuteTask> executeNodeList = new ArrayList<>();
                for (String nodeName : nodeNameList) {
                    FlowNodeInterface detailNode = (FlowNodeInterface) BeanService.getSingleBeanByType(Class.forName(nodeName));
                    NodeExecuteTask nodeParallelTask = new NodeExecuteTask(detailNode, runData, context);
                    executeNodeNameList.add(nodeName);
                    executeNodeList.add(nodeParallelTask);
                    // 提交任务，且接收返回的结果
                    resultList.add(threadPool.submit(nodeParallelTask));
                }
                for (int i = 0; i < resultList.size(); i++) {
                    String nodeName = executeNodeNameList.get(i);
                    String nodeKey = groupName + "_" + nodeName;
                    FlowNodeInterface detailNode = (FlowNodeInterface) BeanService.getSingleBeanByType(Class.forName(nodeName));
                    NodeConf nodeConf = nodeMap.get(nodeKey);
                    int timeout = nodeConf.getTimeout();
                    Future future = resultList.get(i);
                    try {
                        // 检测是否超时
                        Object o = future.get(timeout, TimeUnit.MILLISECONDS);
                        context.getAdptorMap().put(detailNode.resultKey(), o);
                    } catch (Exception e) {
                        needThrowExp = true;
                    }
                }
                if (needThrowExp) {
                    throw new RuntimeException();
                }
            }
        }
    }

    /**
     * 把节点进行分组
     * @param flowNode
     * @return
     */
    private Map<String, List<String>> groupByGroupName(FlowNode flowNode) {
        Map<String, List<String>> nodeGroup = new HashMap<>();
        Set<String> nodeList = flowNode.getNodeList();
        for (String nodeKey : nodeList) {
            String groupName = getGroupName(nodeKey);
            String nodeName = getNodeName(nodeKey);
            if (StringUtils.isBlank(groupName)) {
                List<String> nodeNameList = new ArrayList<>();
                nodeNameList.add(nodeName);
                nodeGroup.put(nodeName, nodeNameList);
            } else {
                List<String> nodeNameList = nodeGroup.get(groupName);
                if (nodeNameList == null) {
                    nodeNameList = new ArrayList<>();
                }
                nodeNameList.add(nodeName);
                nodeGroup.put(groupName, nodeNameList);
            }
        }
        return nodeGroup;

    }

    private String getGroupName(String nodeKey) {
        String[] arr = nodeKey.split("_");
        String s = arr.length == 2 ? arr[0] : null;
        //System.out.println("组名：" + s);
        return s;
    }

    private String getNodeName(String nodeKey) {
        String[] arr = nodeKey.split("_");
        String s = arr.length == 2 ? arr[1] : arr[0];
        //System.out.println("节点名：" + s);
        return s;
    }

    /*public static ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5, 10, 60L,
            TimeUnit.MINUTES, new LinkedBlockingDeque<>(500), new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            return null;
        }
    }, new ThreadPoolExecutor.AbortPolicy() {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            throw new RuntimeException("Task " + r.toString() + " rejected from " + e.toString());
        }
    });*/

    public static ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5, 10, 60L,
            TimeUnit.MINUTES, new LinkedBlockingDeque<>(500));

}
