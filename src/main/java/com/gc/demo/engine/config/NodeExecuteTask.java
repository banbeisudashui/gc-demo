package com.gc.demo.engine.config;

import com.gc.demo.engine.model.Context;
import com.gc.demo.engine.model.RunData;
import com.gc.demo.engine.service.FlowNodeInterface;

import java.util.concurrent.Callable;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/30 15:27
 **/
public class NodeExecuteTask implements Callable {

    private FlowNodeInterface flowNodeInterface;

    private RunData runData;

    private Context context;

    public NodeExecuteTask(FlowNodeInterface flowNodeInterface, RunData runData, Context context) {
        this.flowNodeInterface = flowNodeInterface;
        this.runData = runData;
        this.context = context;
    }

    public Object excute() {
        Object o = flowNodeInterface.invokeNode(runData, context);
        flowNodeInterface.afterInvoke(runData, context);
        return o;
    }

    @Override
    public Object call() throws Exception {
        return excute();
    }
}
