package com.gc.demo.engine.model;

import lombok.Data;

/**
 * @author Leo
 * @description 下游节点配置，超时设置等等
 * @createDate 2021/10/28 22:56
 **/
@Data
public class NodeConf {

    private int timeout = 100;

}
