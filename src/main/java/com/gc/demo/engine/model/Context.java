package com.gc.demo.engine.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Leo
 * @description 创建context上下文，作为调用下游服务的返回结果
 * @createDate 2021/10/28 22:51
 **/
public class Context {

    /**
     * 缓存结果
     */
    private Map<String, Object> resultMap = new HashMap<>();

    /**
     * 获取结果缓存
     * @return
     */
    public Map<String, Object> getAdptorMap() {
        return resultMap;
    }

    public void setAdptorMap(Map<String, Object> resultMap) {
        this.resultMap = resultMap;
    }


}
