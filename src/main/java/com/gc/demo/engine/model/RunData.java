package com.gc.demo.engine.model;

import lombok.Data;

/**
 * @author Leo
 * @description 流程中的参数/接口传参
 * @createDate 2021/10/30 14:41
 **/
@Data
public class RunData {

    private String paramOne;

    private String paramTwo;


}
