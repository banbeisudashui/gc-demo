package com.gc.demo.engine;

import com.gc.demo.engine.config.FlowNode;
import com.gc.demo.engine.model.NodeConf;
import com.gc.demo.engine.service.NodeOne;
import com.gc.demo.engine.service.NodeTwo;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/30 15:52
 **/
public class Flow {

    private static FlowNode testFlow = new FlowNode();

    static {
        // 节点一/组节点1
        testFlow.add(NodeOne.class, new NodeConf());
        // 节点二/组节点2
        testFlow.add(NodeTwo.class, new NodeConf());
        // 组节点3-节点1
        testFlow.add("three", NodeOne.class, new NodeConf());
        // 组节点3-节点2
        testFlow.add("three", NodeTwo.class, new NodeConf());
    }

    public static FlowNode getTestFlow() {
        return testFlow;
    }

}
