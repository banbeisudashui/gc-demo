package com.gc.demo.engine.service;

import com.gc.demo.engine.model.Context;
import com.gc.demo.engine.model.RunData;
import org.springframework.stereotype.Service;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/30 15:45
 **/
@Service
public class NodeOne implements FlowNodeInterface {
    /**
     * 校验逻辑
     * @param nodeData
     * @param context
     * @return
     */
    @Override
    public Object invokeNode(RunData nodeData, Context context) {
        System.out.println("执行了" + nodeData.getParamOne());
        return nodeData.getParamOne();
    }

    @Override
    public void afterInvoke(RunData runData, Context context) {

    }

    @Override
    public String resultKey() {
        return "NodeOne";
    }
}
