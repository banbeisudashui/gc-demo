package com.gc.demo.engine.service;

import com.gc.demo.engine.model.Context;
import com.gc.demo.engine.model.RunData;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/30 15:19
 **/
public interface FlowNodeInterface<T> {

    /**
     * node执行的方法
     * @param nodeData
     * @param context
     * @return
     */
    T invokeNode(RunData nodeData, Context context);

    /**
     * node执行完的方法
     * @param runData
     * @param context
     */
    void afterInvoke(RunData runData, Context context);

    /**
     * 存储节点结果的key
     * @return
     */
    String resultKey();

}
