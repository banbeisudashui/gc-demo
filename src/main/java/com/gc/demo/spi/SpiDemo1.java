package com.gc.demo.spi;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/24 15:28
 **/
public class SpiDemo1 implements SpiDemo {

    @Override
    public String demoMethod(String s) {
        System.out.println("spi的demo1方法");
        return s;
    }
}
