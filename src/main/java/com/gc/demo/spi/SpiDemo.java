package com.gc.demo.spi;

/**
 * @author Leo spi示例
 * @description
 * @createDate 2021/10/24 15:28
 **/
public interface SpiDemo {

    String demoMethod(String s);

}
