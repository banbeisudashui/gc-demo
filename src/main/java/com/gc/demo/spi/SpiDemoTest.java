package com.gc.demo.spi;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/24 15:32
 **/
public class SpiDemoTest {

    public static void main(String[] args) {
        ServiceLoader<SpiDemo> load = ServiceLoader.load(SpiDemo.class);
        Iterator<SpiDemo> iterator = load.iterator();
        int i = 1;
        while (iterator.hasNext()) {
            SpiDemo next = iterator.next();
            String s = next.demoMethod(i + "");
            i++;
            System.out.println(s);
        }
    }

}
