package com.gc.demo.thread.park;

/**
 * @author Leo
 * @description park示例
 * @createDate 2021/8/31 10:39
 **/
public class ParkThread {

    /**
     * park（）是让运行该行代码的线程暂时处于阻塞状态，且需要unpark让线程唤醒
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) {
        Park parkThread = new Park();
        Thread threadPark = new Thread(parkThread);
        threadPark.start();
        UnPark unPark = new UnPark(threadPark);
        Thread threadUnPark = new Thread(unPark);
        threadUnPark.start();
    }
}
