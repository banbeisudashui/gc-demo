package com.gc.demo.thread.park;

import java.util.concurrent.locks.LockSupport;

/**
 * @author Leo
 * @description
 * @createDate 2021/9/4 21:52
 **/
public class UnPark implements Runnable {

    private Thread thread;

    public UnPark(Thread thread) {
        this.thread = thread;
    }

    @Override
    public void run() {
        System.out.println("unPark开始");
        LockSupport.unpark(thread);
        System.out.println("unPark结束");
    }
}