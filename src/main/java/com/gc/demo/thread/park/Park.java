package com.gc.demo.thread.park;
import java.util.concurrent.locks.LockSupport;

/**
 * @author Leo
 * @description
 * @createDate 2021/9/4 21:52
 **/
public class  Park implements Runnable {

    @Override
    public void run() {
        System.out.println("park开始");
        LockSupport.park();
        System.out.println("park结束");
    }
}