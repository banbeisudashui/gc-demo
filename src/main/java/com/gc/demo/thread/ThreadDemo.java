package com.gc.demo.thread;

/**
 * @author Leo
 * @description
 * @createDate 2021/8/30 15:27
 **/
public class ThreadDemo extends Thread {

    private String threadName;

    public ThreadDemo(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(threadName + " " + i);
        }
    }


}
