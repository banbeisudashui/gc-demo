package com.gc.demo.thread.reorder;

/**
 * @author Leo
 * @description 指令重排例子,单线程语义
 * @createDate 2021/9/19 22:01
 **/
public class ReriderTrue {

    public int a = 0;
    public boolean flag = false;

    public void write() {
        a = 1;//1
        flag = true;//2
    }

    public void read() {
        if (flag) {//3
            int i = a * a;//4
            System.out.println(flag + "===" + i);
        }
        System.out.println(flag);
    }

    public static void main(String[] args) {
        ReriderTrue rerider = new ReriderTrue();
        rerider.write();
        rerider.read();
    }

}
