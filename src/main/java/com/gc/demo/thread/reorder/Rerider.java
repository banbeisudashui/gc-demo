package com.gc.demo.thread.reorder;

/**
 * @author Leo
 * @description 指令重排例子
 * @createDate 2021/9/19 22:01
 **/
public class Rerider {

    public int a = 0;
    public boolean flag = false;

    public void write() {
        a = 1;//1
        flag = true;//2
    }

    public void read() {
        if (flag) {//3
            int i = a * a;//4
            System.out.println(flag + "===" + i);
        }
        System.out.println(flag);
    }

    public static void main(String[] args) {
        Rerider rerider = new Rerider();

        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                rerider.write();
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                rerider.read();
            }
        }.start();
    }

}
