package com.gc.demo.thread.join;

/**
 * @author Leo
 * @description join案例，底层实际调用wait
 * @createDate 2021/9/1 16:44
 **/
public class JoinThread implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName() + "..." + i);
        }
    }

    /**
     * 线程的join用法，运行结果是，完全输出线程1结果后，再输出main结果
     * 让权，底层调用的wait方法
     * 不涉及锁
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        JoinThread thread = new JoinThread();
        Thread t1 = new Thread(thread, "join线程");
        for (int j = 0; j < 20; j++) {
            System.out.println(Thread.currentThread().getName() + "..." + j);
            if (j == 10) {
                t1.start();
                t1.join();
            }
        }
    }
}

