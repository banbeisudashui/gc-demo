package com.gc.demo.thread.join;

/**
 * @author Leo
 * @description join-不会释放锁案例，底层实际调用wait
 * @createDate 2021/9/1 16:44
 **/
public class Join2Thread implements Runnable {

    private static Object o = new Object();

    @Override
    public void run() {
        synchronized (o) {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + "..." + i);
            }
        }
    }

    /**
     * 不会释放锁篇
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        Join2Thread thread = new Join2Thread();
        Thread join2 = new Thread(thread, "join2线程");
        join2.start();
        synchronized (o) {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + "..." + i);
                if (i == 1) {
                    join2.join();
                }
            }
        }
    }
}

