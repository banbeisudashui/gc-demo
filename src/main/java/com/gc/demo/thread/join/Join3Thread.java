package com.gc.demo.thread.join;

/**
 * @author Leo
 * @description join-释放锁案例，底层实际调用wait
 * @createDate 2021/9/1 16:44
 **/
public class Join3Thread extends Thread {


    @Override
    public void run() {
        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + "..." + i);
            }
        }
    }

    /**
     * 释放锁篇
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        Join3Thread thread = new Join3Thread();
        thread.setName("join3线程");
        thread.start();
        synchronized (thread) {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + "..." + i);
                if (i == 1) {
                    thread.join();
                }
            }
        }
    }
}

