package com.gc.demo.thread.atmoic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Leo
 * @description
 * @createDate 2021/9/26 22:54
 **/
public class AtmoicDemo implements Runnable {

    private AtomicInteger atomicInteger;

    public AtmoicDemo(AtomicInteger atomicInteger) {
        this.atomicInteger = atomicInteger;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            this.atomicInteger.getAndIncrement();
        }
        System.out.println(Thread.currentThread().getName() + "============" + this.atomicInteger.get());
    }

    public static void main(String[] args) throws InterruptedException {
        AtomicInteger atomicInteger = new AtomicInteger();
        AtmoicDemo demo1 = new AtmoicDemo(atomicInteger);
        AtmoicDemo demo2 = new AtmoicDemo(atomicInteger);
        Thread thread1 = new Thread(demo1, "线程1");
        Thread thread2 = new Thread(demo2, "线程2");
        thread1.start();
        thread2.start();
    }

}
