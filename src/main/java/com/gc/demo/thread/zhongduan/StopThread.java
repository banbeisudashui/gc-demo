package com.gc.demo.thread.zhongduan;

/**
 * 通过stop强制终止线程
 */
public class StopThread implements Runnable {

    private static boolean isFlag = true;

    private static int i = 0;

    @Override
    public void run() {
        while (isFlag) {
            System.out.println(i++);
        }
    }

    public static void main(String[] args) {
        StopThread stopThread = new StopThread();
        Thread thread = new Thread(stopThread);
        thread.start();
        try {
            // 由于过快，所以加上休眠时间，用于打印，效果
            Thread.sleep(5);
            thread.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
