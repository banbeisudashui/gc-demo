package com.gc.demo.thread.zhongduan;

/**
 * 通过Interrupt终止线程,不能强制停止线程，只是通知jvm让该线程停止运行，具体看线程
 */
public class Interrupt2Thread implements Runnable {

    @Override
    public void run() {
        for (int j = 0; j < 100; j++) {
            System.out.println(Thread.currentThread().getName() + "...." + j);
            if (Thread.currentThread().isInterrupted()) {
                System.out.println(Thread.currentThread().getName() + "线程中断");
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();// 恢复这句注释即可运行成功
            }
        }
    }

    public static void main(String[] args) {
        Interrupt2Thread interruptThread = new Interrupt2Thread();
        Thread thread = new Thread(interruptThread,"interruptSleep线程");
        thread.start();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        thread.interrupt();
    }
}
