package com.gc.demo.thread.zhongduan;

/**
 * 通过标志位终止线程
 */
public class FlagThread implements Runnable {

    private static boolean isFlag = true;

    private static int i = 0;

    @Override
    public void run() {
        while (isFlag) {
            System.out.println(i++);
        }
    }

    public static void main(String[] args) {
        FlagThread flagThread = new FlagThread();
        Thread thread = new Thread(flagThread);
        thread.start();
        try {
            // 由于过快，所以加上休眠时间，用于打印，效果
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isFlag = false;
    }
}
