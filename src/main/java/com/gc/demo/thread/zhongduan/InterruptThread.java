package com.gc.demo.thread.zhongduan;

/**
 * 通过Interrupt终止线程,不能强制停止线程，只是通知jvm让该线程停止运行，具体看线程
 */
public class InterruptThread implements Runnable {

    @Override
    public void run() {
        for (int j = 0; j < 100; j++) {
            System.out.println(j);
        }
    }

    public static void main(String[] args) {
        InterruptThread interruptThread = new InterruptThread();
        Thread thread = new Thread(interruptThread);
        thread.start();
        try {
            thread.interrupt();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
