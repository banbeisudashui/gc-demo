package com.gc.demo.thread.dieLock;

/**
 * @author Leo
 * @description
 * @createDate 2021/8/30 22:21
 **/
public class DieLock implements Runnable {
    private boolean flag;

    public DieLock(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void run() {
        if (flag) {
            while (true) {
                synchronized (MyLock.obj1) {
                    System.out.println(Thread.currentThread().getName() + "....if...obj1..." + flag);
                    synchronized (MyLock.obj2) {
                        System.out.println(Thread.currentThread().getName() + ".....if.....obj2....." + flag);

                    }
                }
            }
        } else {
            while (true) {
                synchronized (MyLock.obj2) {
                    System.out.println(Thread.currentThread().getName() + "....else...obj2..." + flag);
                    synchronized (MyLock.obj1) {
                        System.out.println(Thread.currentThread().getName() + ".....else.....obj1....." + flag);

                    }
                }
            }
        }

    }
}
