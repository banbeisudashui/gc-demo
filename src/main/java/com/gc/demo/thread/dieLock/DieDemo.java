package com.gc.demo.thread.dieLock;

/**
 * @author Leo
 * @description
 * @createDate 2021/9/4 21:16
 **/
public class DieDemo {

    public static void main(String[] args) {
        DieLock d1 = new DieLock(true);
        DieLock d2 = new DieLock(false);
        Thread t1 = new Thread(d1);
        Thread t2 = new Thread(d2);
        t1.start();
        t2.start();
    }

}
