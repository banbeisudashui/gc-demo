package com.gc.demo.thread;

/**
 * @author Leo
 * @description notifyAll示例
 * @createDate 2021/8/31 10:39
 **/
public class NotifyAllThread implements Runnable {

    private String z = "锁";

    @Override
    public void run() {
        synchronized (z) {
            for (int i = 0; i < 20; i++) {
                try {
                    z.notifyAll();
                    System.out.println(Thread.currentThread().getName() + "...." + i);
                    z.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * notifyAll（）是让运行该行代码的线程处于wait状态，争夺锁
     * main线程运行A、B、C线程，由于A、B、C线程持有同一个锁，所以导致相互释放锁，争夺锁，导致死锁
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) {
        NotifyAllThread notifyThread1 = new NotifyAllThread();
        NotifyAllThread notifyThread2 = new NotifyAllThread();
        NotifyAllThread notifyThread3 = new NotifyAllThread();
        Thread thread1 = new Thread(notifyThread1, "notify1线程");
        Thread thread2 = new Thread(notifyThread2, "notify2线程");
        Thread thread3 = new Thread(notifyThread3, "notify3线程");
        thread1.start();
        thread2.start();
        thread3.start();
        return;
    }
}
