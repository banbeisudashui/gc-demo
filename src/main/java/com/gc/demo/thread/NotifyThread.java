package com.gc.demo.thread;

/**
 * @author Leo
 * @description notify示例
 * @createDate 2021/8/31 10:39
 **/
public class NotifyThread implements Runnable {

    private String z = "锁";

    @Override
    public void run() {
        synchronized (z) {
            for (int i = 0; i < 20; i++) {
                try {
                    z.notify();
                    System.out.println(Thread.currentThread().getName() + "...." + i);
                    z.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * notify（）是让运行该行代码的线程处于wait状态，释放锁
     * main线程运行A、B线程，由于A、B线程持有同一个锁，所以导致相互释放锁，争夺锁
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) {
        NotifyThread notifyThread1 = new NotifyThread();
        NotifyThread notifyThread2 = new NotifyThread();
        Thread thread1 = new Thread(notifyThread1, "notify1线程");
        Thread thread2 = new Thread(notifyThread2, "notify2线程");
        thread1.start();
        thread2.start();
        return;
    }
}
