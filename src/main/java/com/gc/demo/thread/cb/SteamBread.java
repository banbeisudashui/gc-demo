package com.gc.demo.thread.cb;

/**
 * @author Leo
 * @description 商品
 * @createDate 2021/9/4 16:07
 **/
public class SteamBread {
    int id;

    public SteamBread(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SteamBread{" +
                "id=" + id +
                '}';
    }
}
