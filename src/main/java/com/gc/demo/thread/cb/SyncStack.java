package com.gc.demo.thread.cb;

/**
 * @author Leo
 * @description 货架，最多只放六件商品
 * @createDate 2021/9/4 16:08
 **/
public class SyncStack {
    int index = 0;

    SteamBread[] stb = new SteamBread[6];

    public synchronized void push(SteamBread steamBread) {
        while (index == stb.length) {
            try {
                this.wait();
            } catch (Exception e) {

            } finally {

            }
        }
        this.notify();
        stb[index] = steamBread;
        this.index++;
    }

    public synchronized SteamBread pop() {
        while (index == 0) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.notify();
        this.index--;
        return stb[index];
    }
}
