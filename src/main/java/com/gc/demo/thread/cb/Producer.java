package com.gc.demo.thread.cb;

/**
 * @author Leo
 * @description 生产者
 * @createDate 2021/9/4 16:08
 **/
public class Producer implements Runnable {

    SyncStack ss = null;

    public Producer(SyncStack ss) {
        this.ss = ss;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            SteamBread steamBread = new SteamBread(i);
            ss.push(steamBread);
            System.out.println("生产了" + steamBread.toString());
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
