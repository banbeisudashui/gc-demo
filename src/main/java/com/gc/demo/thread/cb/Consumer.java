package com.gc.demo.thread.cb;

/**
 * @author Leo
 * @description 消费者
 * @createDate 2021/9/4 16:09
 **/
public  class Consumer implements Runnable {

    SyncStack ss = null;

    public Consumer(SyncStack ss) {
        this.ss = ss;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            SteamBread steamBread = new SteamBread(i);
            ss.pop();
            System.out.println("消费了" + steamBread.toString());
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
