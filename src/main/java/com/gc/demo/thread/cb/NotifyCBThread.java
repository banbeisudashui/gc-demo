package com.gc.demo.thread.cb;

/**
 * @author Leo
 * @description 经典消费者，生产者多线程
 * @createDate 2021/8/31 10:39
 **/
public class NotifyCBThread {

    public static void main(String[] args) {
        SyncStack syncStack = new SyncStack();
        Consumer consumer = new Consumer(syncStack);
        Producer producer = new Producer(syncStack);
        Thread cT = new Thread(consumer);
        Thread pT = new Thread(producer);
        pT.start();
        cT.start();
    }


}


