package com.gc.demo.thread;

/**
 * @author Leo
 * @description yield示例
 * @createDate 2021/8/31 10:39
 **/
public class YieldThread implements Runnable {

    private String z = "锁";

    @Override
    public void run() {
        synchronized (z) {
            for (int i = 0; i < 20; i++) {
                try {
                    System.out.println(Thread.currentThread().getName() + "...." + i);
                    Thread.yield();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * yield（）是让运行该行代码的线程暂时处于wait状态（），让锁出来，但也有可能会马上获取到锁
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) {
        YieldThread yieldThread1 = new YieldThread();
        YieldThread yieldThread2 = new YieldThread();
        YieldThread yieldThread3 = new YieldThread();
        Thread thread1 = new Thread(yieldThread1, "yield1线程");
        Thread thread2 = new Thread(yieldThread2, "yield2线程");
        Thread thread3 = new Thread(yieldThread3, "yield3线程");
        thread1.start();
        thread2.start();
        thread3.start();
    }
}
