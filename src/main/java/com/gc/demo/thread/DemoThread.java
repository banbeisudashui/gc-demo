package com.gc.demo.thread;

/**
 * @author Leo
 * @description
 * @createDate 2021/8/30 15:26
 **/
public class DemoThread {

    public static void main(String[] args) {
        /*RunnableDemo runnableDemo1 = new RunnableDemo("T1");
        RunnableDemo runnableDemo2 = new RunnableDemo("T2");
        Thread thread1 = new Thread(runnableDemo1);
        Thread thread2 = new Thread(runnableDemo2);
        thread1.start();
        thread2.start();*/

        ThreadDemo demo1 = new ThreadDemo("T1");
        ThreadDemo demo2 = new ThreadDemo("T2");
        demo1.start();
        demo2.start();

    }

}
