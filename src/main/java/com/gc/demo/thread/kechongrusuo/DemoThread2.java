package com.gc.demo.thread.kechongrusuo;

/**
 * @author Leo
 * @description 可重入锁案例2，synchronize示例
 * @createDate 2021/9/7 22:40
 **/
public class DemoThread2 implements Runnable {

    private static Object o = new Object();

    public static void main(String[] args) {
        DemoThread2 demoThread2 = new DemoThread2();
        Thread thread = new Thread(demoThread2);
        thread.start();
    }

    public synchronized void method1(Object o) {
        System.out.println("method1" + o.toString());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void method2(Object o) {
        System.out.println("method2" + o.toString());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void method3(Object o) {
        System.out.println("method3" + o.toString());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        synchronized (o) {
            System.out.println("method开始" + o.toString());
            method1(o);
            method2(o);
            method3(o);
            System.out.println("method结束" + o.toString());
        }
    }
}
