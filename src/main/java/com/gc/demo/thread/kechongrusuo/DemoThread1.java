package com.gc.demo.thread.kechongrusuo;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Leo
 * @description 可重入锁案例1，lock示例
 * @createDate 2021/9/7 22:40
 **/
public class DemoThread1 {

    private static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        method1();
    }

    public static void method1() {
        lock.lock();
        try {
            System.out.println("method1");
            method2();
        } catch (Exception e) {

        } finally {
            lock.unlock();
        }
    }

    public static void method2() {
        lock.lock();
        try {
            System.out.println("method2");
            method3();
        } catch (Exception e) {

        } finally {
            lock.unlock();
        }
    }

    public static void method3() {
        lock.lock();
        try {
            System.out.println("method3");
        } catch (Exception e) {

        } finally {
            lock.unlock();
        }
    }

}
