package com.gc.demo.thread;

import static java.lang.Thread.currentThread;

/**
 * @author Leo
 * @description wait()案例
 * @createDate 2021/8/31 10:39
 **/
public class WaitThread implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(currentThread().getName() + "...." + i);
        }
    }

    /**
     * wait（）是让运行该行代码的线程处于wait状态，释放锁
     * main线程运行A线程，当运行到thread.wait（）改行代码时，main线程处于wait状态，让A线程运行
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        WaitThread waitThread = new WaitThread();
        Thread thread = new Thread(waitThread, "等待线程");
        synchronized (thread) {
            for (int i = 0; i < 20; i++) {
                if (i == 10) {
                    thread.start();
                    thread.wait();
                }
                System.out.println(currentThread().getName() + "...." + i);
            }
        }
    }
}
