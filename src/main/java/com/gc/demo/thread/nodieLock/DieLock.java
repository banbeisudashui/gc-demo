package com.gc.demo.thread.nodieLock;

/**
 * @author Leo
 * @description
 * @createDate 2021/8/30 22:21
 **/
public class DieLock implements Runnable {

    @Override
    public void run() {
        while (true) {
            synchronized (MyLock.obj1) {
                System.out.println(Thread.currentThread().getName() + "....if...obj1..." );
                synchronized (MyLock.obj2) {
                    System.out.println(Thread.currentThread().getName() + ".....if.....obj2....." );

                }
            }
        }
    }
}
