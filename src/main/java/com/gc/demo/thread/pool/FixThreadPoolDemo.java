package com.gc.demo.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author Leo
 * @description 固定线程池demo
 * @createDate 2021/10/1 21:15
 **/
public class FixThreadPoolDemo {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
    }

}
