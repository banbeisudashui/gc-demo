package com.gc.demo.thread;

/**
 * @author Leo
 * @description sleep示例
 * @createDate 2021/8/31 10:39
 **/
public class SleepThread implements Runnable {

    private String z = "锁";

    @Override
    public void run() {
        synchronized (z) {
            for (int i = 0; i < 20; i++) {
                try {
                    System.out.println(Thread.currentThread().getName() + "...." + i);
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * sleep（）是让运行该行代码的线程暂时处于wait状态（），但并不会释放锁
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) {
        SleepThread sleepThread1 = new SleepThread();
        SleepThread sleepThread2 = new SleepThread();
        SleepThread sleepThread3 = new SleepThread();
        Thread thread1 = new Thread(sleepThread1, "sleep1线程");
        Thread thread2 = new Thread(sleepThread2, "sleep2线程");
        Thread thread3 = new Thread(sleepThread3, "sleep3线程");
        thread1.start();
        thread2.start();
        thread3.start();
    }
}
