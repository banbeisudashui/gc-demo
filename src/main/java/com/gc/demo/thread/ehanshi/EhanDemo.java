package com.gc.demo.thread.ehanshi;

/**
 * @author Leo
 * @description 饿汉式对象,线程不安全
 * @createDate 2021/9/18 15:32
 **/
public class EhanDemo {

    private static EhanDemo instance = null;

    private EhanDemo() {
    }

    public static EhanDemo getInstance() {
        if (instance == null) {
            instance = new EhanDemo();
        }
        return instance;
    }
}
