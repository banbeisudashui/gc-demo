package com.gc.demo.thread.ehanshi;

/**
 * @author Leo
 * @description 饿汉式对象,线程安全
 * @createDate 2021/9/18 15:32
 **/
public class EhanDemo2 {

    private static EhanDemo2 instance = null;

    private EhanDemo2() {
    }

    public static synchronized EhanDemo2 getInstance() {
        if (instance == null) {
            instance = new EhanDemo2();
        }
        return instance;
    }
}
