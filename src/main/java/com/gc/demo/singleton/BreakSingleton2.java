package com.gc.demo.singleton;

import java.io.*;
import java.lang.reflect.Constructor;

/**
 * @author Leo
 * @description 利用序列化和反序列化破坏单例模式
 * @createDate 2021/10/24 22:31
 **/
public class BreakSingleton2 {

    public static void main(String[] args) throws Exception {
        // 创建输出流
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Singleton.file"));
        // 将单例对象写到文件中
        oos.writeObject(Esingleton.getEsingleton());
        // 从文件中读取单例对象
        File file = new File("Singleton.file");
        ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
        Esingleton newInstance = (Esingleton) ois.readObject();
        // 判断是否是同一个对象
        System.out.println(newInstance == Esingleton.getEsingleton()); // false
    }


}
