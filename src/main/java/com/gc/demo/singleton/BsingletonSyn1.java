package com.gc.demo.singleton;

/**
 * @author Leo
 * @description 饱汉式单例模式-线程安全1
 * @createDate 2021/10/24 22:05
 **/
public class BsingletonSyn1 {

    private static BsingletonSyn1 bsingleton  = null;

    private BsingletonSyn1() {
    }

    public static synchronized BsingletonSyn1 getBsingleton(){
        if(bsingleton==null){
            bsingleton = new BsingletonSyn1();
        }
        return bsingleton;
    }
}
