package com.gc.demo.singleton;

/**
 * @author Leo
 * @description 饿汉式单例模式
 * @createDate 2021/10/24 22:05
 **/
public class Esingleton {

    private static final Esingleton esingleton = new Esingleton();

    private Esingleton() {
    }

    public static Esingleton getEsingleton(){
        return esingleton;
    }
}
