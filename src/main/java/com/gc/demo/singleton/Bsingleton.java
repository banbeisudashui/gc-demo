package com.gc.demo.singleton;

/**
 * @author Leo
 * @description 饱汉式单例模式-线程不安全版
 * @createDate 2021/10/24 22:05
 **/
public class Bsingleton {

    private static  Bsingleton bsingleton  = null;

    private Bsingleton() {
    }

    public static Bsingleton getBsingleton(){
        if(bsingleton==null){
            bsingleton = new Bsingleton();
        }
        return bsingleton;
    }
}
