package com.gc.demo.singleton;

/**
 * @author Leo
 * @description 饱汉式单例模式-线程安全2
 * @createDate 2021/10/24 22:05
 **/
public class BsingletonSyn3 {

    private static volatile BsingletonSyn3 bsingleton  = null;

    private BsingletonSyn3() {
    }

    public static synchronized BsingletonSyn3 getBsingleton(){
        if(bsingleton==null){
            synchronized (BsingletonSyn3.class){
                bsingleton = new BsingletonSyn3();
            }
        }
        return bsingleton;
    }
}
