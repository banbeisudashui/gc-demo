package com.gc.demo.singleton;

import java.lang.reflect.Constructor;

/**
 * @author Leo
 * @description 利用反射破坏单例模式
 * @createDate 2021/10/24 22:31
 **/
public class BreakSingleton {

    public static void main(String[] args) throws Exception {
        Constructor<Esingleton> declaredConstructor = Esingleton.class.getDeclaredConstructor();
        declaredConstructor.setAccessible(true);
        Esingleton esingleton = declaredConstructor.newInstance();
        Esingleton esingleton1 = Esingleton.getEsingleton();
        System.out.println(esingleton == esingleton1);
    }

}
