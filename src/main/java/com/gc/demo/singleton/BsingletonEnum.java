package com.gc.demo.singleton;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/24 22:27
 **/
public enum BsingletonEnum {
    /**
     * 定义一个元素，它就是代表了singleton的一个实例
     */
    instance

}
