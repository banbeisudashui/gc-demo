package com.gc.demo.singleton;

/**
 * @author Leo
 * @description 饱汉式单例模式-线程安全2
 * @createDate 2021/10/24 22:05
 **/
public class BsingletonSyn2 {

    private static BsingletonSyn2 bsingleton  = null;

    private BsingletonSyn2() {
    }

    public static synchronized BsingletonSyn2 getBsingleton(){
        if(bsingleton==null){
            synchronized (BsingletonSyn2.class){
                bsingleton = new BsingletonSyn2();
            }
        }
        return bsingleton;
    }
}
