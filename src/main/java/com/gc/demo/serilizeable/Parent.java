package com.gc.demo.serilizeable;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/25 22:37
 **/
public class Parent {

    private int classNum = 100;



    public Parent(int classNum) {
        this.classNum = classNum;
    }

    public Parent() {

    }

    public int getClassNum() {
        return classNum;
    }

    public void setClassNum(int classNum) {
        this.classNum = classNum;
    }
}
