package com.gc.demo.serilizeable;

import java.io.*;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/25 22:22
 **/
public class SerialStaticTest implements Serializable {

    private static final long serialVersionUID = 1L;

    public static int staticVar = 5;

    public static void main(String[] args) {
        try {
            File file = new File("C:\\Users\\Administrator\\Desktop\\student.txt");
            try {
                file.createNewFile();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
            //初始时staticVar为5
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(file));
            out.writeObject(new SerialStaticTest());
            out.close();

            //序列化后修改为10
            SerialStaticTest.staticVar = 10;

            ObjectInputStream oin = new ObjectInputStream(new FileInputStream(file));
            SerialStaticTest t = (SerialStaticTest) oin.readObject();
            oin.close();

            //再读取，通过t.staticVar打印新的值
            System.out.println(t.staticVar);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

