package com.gc.demo.serilizeable;

import java.io.Serializable;

/**
 * @author Leo
 * @description
 * @createDate 2021/10/25 22:37
 **/
public class Child1 extends Parent implements Serializable  {

    private String name;

    public Child1(String name) {
        super();
        this.name = name;
    }

    public Child1(int classNum, String name) {
        super(classNum);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
