package com.gc.demo.serilizeable;

import java.io.*;

/**
 * @author Leo
 * @description 父类和子类序列化示例
 * 父类没有继承序列化接口
 * 子类继承序列化接口
 * @createDate 2021/10/25 22:03
 **/
public class DemoTest2 {

    public static void main(String[] args) {
        Child1 st = new Child1(200,"Tom");
        File file = new File("C:\\Users\\Administrator\\Desktop\\Child1.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //Student对象序列化过程
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(st);
            oos.flush();
            oos.close();
            fos.close();

            //Student对象反序列化过程
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Child1 st1 = (Child1) ois.readObject();
            System.out.println("name = " + st1.getName());
            System.out.println("name = " + st1.getClassNum());
            ois.close();
            fis.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
