package com.gc.demo.serilizeable;

import java.io.*;

/**
 * @author Leo
 * @description 序列化和再次序列化后的文件大小对比
 * @createDate 2021/10/25 22:49
 **/
public class SerialSaveTest implements Serializable {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\Administrator\\Desktop\\SerialSaveTest.txt");
        try {
            file.createNewFile();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            SerialSaveTest test = new SerialSaveTest();
            //试图将对象两次写入文件
            out.writeObject(test);
            out.flush();
            System.out.println(file.length());
            out.writeObject(test);
            out.close();
            System.out.println(file.length());

            ObjectInputStream oin = new ObjectInputStream(new FileInputStream(file));
            //从文件依次读出两个文件
            SerialSaveTest t1 = (SerialSaveTest) oin.readObject();
            SerialSaveTest t2 = (SerialSaveTest) oin.readObject();
            oin.close();

            //判断两个引用是否指向同一个对象
            System.out.println(t1 == t2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

