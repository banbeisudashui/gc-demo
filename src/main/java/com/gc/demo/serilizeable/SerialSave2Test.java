package com.gc.demo.serilizeable;

import java.io.*;

/**
 * @author Leo
 * @description 序列化后修改属性值，再次序列化，反序列化后
 * @createDate 2021/10/25 22:49
 **/
public class SerialSave2Test implements Serializable {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\Administrator\\Desktop\\SerialSaveTest.txt");
        try {
            file.createNewFile();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            SerialSave2Test test = new SerialSave2Test();
            //试图将对象两次写入文件
            out.writeObject(test);
            out.flush();
            System.out.println(file.length());
            out.writeObject(test);
            out.close();
            System.out.println(file.length());

            ObjectInputStream oin = new ObjectInputStream(new FileInputStream(file));
            //从文件依次读出两个文件
            SerialSave2Test t1 = (SerialSave2Test) oin.readObject();
            SerialSave2Test t2 = (SerialSave2Test) oin.readObject();
            oin.close();

            //判断两个引用是否指向同一个对象
            System.out.println(t1 == t2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

